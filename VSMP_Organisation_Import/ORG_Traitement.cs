﻿using System;
using System.Collections.Generic;
using CRM_Connection;
using Log;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Xml;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VSMP_Organisation
{
    public class ORG_Traitement
    {
        #region variables statique privées
        private static OrganizationServiceProxy _orgService = null;
        private static string EndDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ"); //s'assurer que le format doit etre en utc
        private static string startDate;
        private static string URL;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings;
        private static List<Organisation> Source_Orga_BU;
        private static List<Entity> CRMBusinessUnits=null;
        private static List<Entity> CRM_BU_ToUpdate=null;
        private static List<Entity> CRMContracts = null;
        private static List<Entity> CRMContractsToUpdate = null;
        private static List<Entity> CRMContractsAttached=null;
        private static List<Entity> CRMTeams = null;
        private static EntityCollection BusinessUnitsCollection;
        private static EntityCollection ContractsCollection;
        private static string BP;
        private static string BC;
        private static string ZM;
        private static string DR;
        private static string DEX;
        private static string[] ContractFields = new string[] { "vsmp_name", "vsmp_coderegate", "ownerid", "vsmp_bureaudeposte", "vsmp_secteur", "vsmp_zonedemarche",
                                                                "vsmp_directionregionale", "vsmp_directionexecutive"};
        private static string[] TeamFields = new string[] { "name","businessunitid" };
        private static string[] BUFields = new string[] { "vsmp_code", "name", "vsmp_typededivision", "parentbusinessunitid" };
        #endregion

        #region Batch Excecute
        public static void BatchExecute(string[] args)
        {
            try
            {
                //1-Créaations des logs
                LogHelper.CreateLog();

                //2-connexion CRM et récupération des records
                _orgService = CRMConnection.InitCRMConnection();
                if (_orgService == null) return;
                
                //3-Récupération des paramètres de configuration
                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);
                XmlNode startDateNode = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Organisation_Settings//OrganisationStartDate");
                startDate = startDateNode.InnerText;
                URL = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Organisation_Settings//WS_Organisation_URL").InnerText;

                //4-Import de l'organisation
                ImportOrganisation();              
            }
            catch(Exception ex)
            {
                LogHelper.Writer("Erreur inattendue lors de l'éxécution. Type d'erreur : " + Environment.NewLine + ex.ToString());
            }
        }
        #endregion

        #region Import de l'organisation
        public static void ImportOrganisation()
        {
            LogHelper.WriteHeader("Nouvelle exécution, Debut du traitement... ");
            Task<string> WS_task = ORG_WS_Connection.GetContracts(URL, startDate, EndDate);
            string result = WS_task.Result;
            Source_Orga_BU = ORG_Fields_Mapping.MapData(result); //Mettre result
            if (Source_Orga_BU != null && Source_Orga_BU.Count != 0 && Source_Orga_BU[0] != null)
            {
                RecordsRetrieve(); //récupération des tous les records
                OrganisationUpdate(); //mise à jour de l'organisation

                BusinessUnitsCollection = new EntityCollection();
                ContractsCollection = new EntityCollection();
                LogHelper.Writer(string.Format("Préparation de l'upsert, {0} BU à créer, {1} BU à mettre à jour...", CRM_BU_ToUpdate.Count(entity => entity.Id == Guid.Empty).ToString(), 
                                                CRM_BU_ToUpdate.Count(entity => entity.Id != Guid.Empty).ToString()));
                
                UpsertRecords(CRM_BU_ToUpdate, BusinessUnitsCollection, "businessunit"); //upsert des divisions

                LogHelper.Writer(string.Format("Préparation de l'upsert, {0} contrats à mettre à jour...", CRMContractsToUpdate.Count().ToString()));
                UpsertRecords(CRMContractsToUpdate, ContractsCollection, "vsmp_contrat"); //upsert des contrats rattachés

                VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Organisation_Settings//OrganisationStartDate").InnerText = EndDate; //mise a jour de la date pour le prochain appel
                VSMP_Settings.Save(Xml_Settings_Path);
            }
            else
                LogHelper.Writer("Aucune division disponible");
        }
        #endregion  

        #region Récupération des records
        static void RecordsRetrieve()
        {
            LogHelper.WriteHeader("Début de récupération des records de toutes les entités depuis le CRM...");

            CRMBusinessUnits = ORG_Application.GetRecords(_orgService, "businessunit", BUFields);   //récupération de toutess BU CRM
            LogHelper.Writer(CRMBusinessUnits.Count + " divisions récupérés.");
           
            CRMContracts = ORG_Application.GetRecords(_orgService, "vsmp_contrat", ContractFields);            //récupération de tous les contrats CRM
            LogHelper.Writer(CRMContracts.Count + " contrats récupérés.");

            CRMTeams = ORG_Application.GetRecords(_orgService, "team", TeamFields);          //récupération de toutes les connexions CRM
            LogHelper.Writer(CRMTeams.Count + " équipes récupérés.");
            
            if (CRMContracts == null) CRMContracts = new List<Entity>();
            if (CRMTeams == null) CRMTeams = new List<Entity>();
            if (CRMBusinessUnits == null) CRMBusinessUnits = new List<Entity>();
            LogHelper.WriteHeader("Fin de récupération des records.");
           
        }
        #endregion

        #region Mise à jour de l'organisation
        static void OrganisationUpdate()
        {
            CRM_BU_ToUpdate = new List<Entity>();
            CRMContractsToUpdate = new List<Entity>();
            LogHelper.WriteHeader("Début du traitement de génération de l'organisation... ");
            foreach (Organisation organisation in Source_Orga_BU)
            {               
                if (organisation.CODE_REGATE != string.Empty && (organisation.CODE_REGATE_ENTITE_RATTACHEMENT != string.Empty || organisation.LIBELLE_COURT_TYPE =="DEX")
                    && organisation.CODE_REGATE != organisation.CODE_REGATE_ENTITE_RATTACHEMENT && organisation.LIBELLE_COURT_TYPE != organisation.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT)
                {
                    if (ORG_Application.GetBU(organisation.CODE_REGATE_ENTITE_RATTACHEMENT, CRMBusinessUnits)!= Guid.Empty)                        
                        GenererDivision(organisation);                            
                    else
                       LogHelper.Writer(string.Format("La Division mère de la {0} : {1} n'existe pas sur le CRM",organisation.LIBELLE_COURT_TYPE,organisation.LIBELLE));
                }
                else
                   LogHelper.Writer("Division non valide extraite depuis Source_Organisation, sois un code regate est manquant ou bien le lien de hiérarchie n'est pas compatible");
            }
            LogHelper.WriteHeader("Fin de la génération de l'organisation");
        }
        #endregion

        #region Géneration de divisions
        static void GenererDivision(Organisation organisation)
        {
            try
            {
                Entity RetrievedBU = CRMBusinessUnits.Find(entity => entity.GetAttributeValue<string>("vsmp_code") == organisation.CODE_REGATE);
                if (RetrievedBU != null)
                {
                    ORG_Application.CreateOrUpdateBU(RetrievedBU, organisation, CRMBusinessUnits);
                    CRM_BU_ToUpdate.Add(RetrievedBU);
                    Update_Attached_Contracts(RetrievedBU);
                }
                else
                {
                    Entity NewBU = new Entity("businessunit");
                    ORG_Application.CreateOrUpdateBU(NewBU, organisation, CRMBusinessUnits);
                    CRM_BU_ToUpdate.Add(NewBU);
                }
            }
            catch(Exception ex)
            {
                LogHelper.Writer("Erreur lors de la génération d'une organisation. Type d'erreur : " + Environment.NewLine + ex.ToString());
            }
        }
        #endregion

        #region traitement des contrats rattachés
        static void Update_Attached_Contracts(Entity RetrievedBU)
        {
            try
            {
                Entity OwnerTeam = CRMTeams.Find(entity => ((EntityReference)entity["businessunitid"]).Id == RetrievedBU.Id);
                CRMContractsAttached = CRMContracts.FindAll(entity => ((EntityReference)entity["ownerid"]).Id == OwnerTeam.Id); 
                if (CRMContractsAttached != null && CRMContractsAttached.Count!=0) 
                {
                    BC = string.Empty;
                    BP = string.Empty;
                    ZM = string.Empty;
                    DR = string.Empty;
                    DEX = string.Empty;
                   
                    if (RetrievedBU.Contains("name") && (string)RetrievedBU["name"] != string.Empty && RetrievedBU.Contains("vsmp_typededivision"))
                    {
                        switch (((OptionSetValue)RetrievedBU["vsmp_typededivision"]).Value)  //les contrats sont seulement rattachés à des BP ou BC
                        {
                            case 688760003:
                                BC = (string)RetrievedBU["name"];
                                break;
                            case 688760004:
                                BP = (string)RetrievedBU["name"];
                                break;
                        }
                    }
                    //on reconsitue la hiérarchie seulement si la bu propriétaire du contrat possède un parent
                    if (RetrievedBU.Contains("parentbusinessunitid") && (EntityReference)RetrievedBU["parentbusinessunitid"] != null)
                        HierarchyFind((EntityReference)RetrievedBU["parentbusinessunitid"]);
                    UpdateHierarchyFields(CRMContractsAttached);
                }              
            }
            catch(Exception ex)
            {
                LogHelper.Writer("Erreur lors de la mise à jour de la hiérarchie dans les contrats, type d'erreur : " + Environment.NewLine + ex.ToString());
            }
        }
        #endregion

        #region Reconstitution de la hiérarche
        static void HierarchyFind(EntityReference Parent)
        {
            try
            {
                Entity ParentBU = CRMBusinessUnits.Find(entity => entity.Id == Parent.Id);

                if (ParentBU.Contains("name") && ParentBU.Contains("vsmp_typededivision"))
                {
                    switch (((OptionSetValue)ParentBU["vsmp_typededivision"]).Value) //la division supérieure à un contrat rattaché peut etre soit un BC, ZM, DR ou DEX
                    {
                        case 688760003: //BC
                            BC = (string)ParentBU["name"];
                            break;
                        case 688760002: //ZM
                            ZM = (string)ParentBU["name"];
                            break;
                        case 688760001: // DR
                            DR = (string)ParentBU["name"];
                            break;
                        case 688760000: // DEX
                            DEX = (string)ParentBU["name"];
                            break;
                    }
                    if (ParentBU.Contains("parentbusinessunitid") && (EntityReference)ParentBU["parentbusinessunitid"] != null && ((OptionSetValue)ParentBU["vsmp_typededivision"]).Value != 688760000)
                        HierarchyFind((EntityReference)ParentBU["parentbusinessunitid"]);
                }
            }
            catch(Exception ex)
            {
                LogHelper.Writer("Erreur lors de la recherche de la hiérarchie, type d'erreur : " + ex.ToString());
            }
        }

        #endregion

        #region Mise à jour des contrats
        static void UpdateHierarchyFields(List<Entity> CRMContractsAttached)
        {
            foreach(Entity contrat in CRMContractsAttached)
            {
                if (BP != string.Empty)
                    contrat.Attributes["vsmp_bureaudeposte"] = BP;
                if (BC != string.Empty)
                    contrat.Attributes["vsmp_secteur"] = BC;
                if (ZM != string.Empty)
                    contrat.Attributes["vsmp_zonedemarche"] = ZM;
                if (DR != string.Empty)
                    contrat.Attributes["vsmp_directionregionale"] = DR;
                if (DEX != string.Empty)
                    contrat.Attributes["vsmp_directionexecutive"] = DEX;           
            }
            CRMContractsToUpdate.AddRange(CRMContractsAttached);
        }
        #endregion

        #region upsert des records
        static void UpsertRecords(List<Entity> records, EntityCollection collection, string logicalName)
        {

            if (records != null && records.Count != 0)
            {
                foreach (Entity entity in records)
                    collection.Entities.Add(entity);
                collection.EntityName = logicalName;
                //Upsert de records sur CRM              
                ORG_Application.CreateMultiple(_orgService, collection);
            }
            else
                LogHelper.Writer("Upsert Impossible : Aucun record trouvé dans la collection d'entités de type : " + logicalName);
        }
        #endregion
    }
}


