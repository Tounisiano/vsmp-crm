﻿using System;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.IO;
using Log;
using CRM_Connection;

namespace VSMP_Contracts
{
    public static class CTR_Traitement
    {
        #region Variables privées  
        private static List<Result> contrats=new List<Result>();                                 //liste de contrats    
        private static Guid id = Guid.Empty;                                  //identificateur unique d'un contrat
        private static Guid idSubscriber = Guid.Empty;                        //identificateur unique d'un contact souscripteur
        private static Guid idRecipient = Guid.Empty;                         //identificateur unique d'un contact bénéf
        private static Guid idTrustedContact = Guid.Empty;                    //identificateur unique d'un contact p.confiance
        private static Guid ContSubConnectionId = Guid.Empty;                 //identificateur unique d'une connexion contrat/souscripteur
        private static Guid ContRecConnectionId = Guid.Empty;                 //identificateur unique d'une connexion contrat/bénéf
        private static Guid ContTrustConnectionId = Guid.Empty;               //identificateur unique d'une connexion contrat/p.confiance
        private static Guid recsubConnectionId = Guid.Empty;                  //identificateur unique d'une connexion bénéf/souscripteur
        private static Guid recTrustConnectionId = Guid.Empty;                //identificateur unique d'une connexion bénéf/p.confiance
        private static string[] ContactFields = new string[] {    "gendercode", "firstname", "lastname", "vsmp_identifiantclient", "vsmp_clientnonnumerique",
                                                                  "vsmp_datedenaissance", "telephone1", "mobilephone", "emailaddress1", "address1_composite",
                                                                  "vsmp_civilitemedecin", "vsmp_nomdumedecin", "vsmp_telephonemedecin", "vsmp_typedinstallationtelephonique",
                                                                  "vsmp_possedeundoubledescles", "vsmp_dureedutrajet", "address1_line1", "address1_line2", "address1_line3", "vsmp_address1_line4",
                                                                  "address1_city", "address1_postalcode", "address1_country"};
        private static string[] ContractFields = new string[] { "vsmp_name", "vsmp_testcontract",  "vsmp_bic", "vsmp_iban", "vsmp_veille", "vsmp_payeur", "vsmp_referenceapport", "vsmp_datedactivation",
                                                                   "vsmp_datedefin", "vsmp_datederesiliation", "vsmp_datedecreation", "vsmp_medaillon", "vsmp_datedannulation", "vsmp_datedesuspension","vsmp_datedesouscription", "vsmp_demarragedelaprestation",
                                                                   "vsmp_typedepaiement", "vsmp_statut", "vsmp_raisondannulation", "vsmp_typedetarif",
                                                                   "vsmp_raisondelasuspension", "vsmp_version","vsmp_prixttc", "vsmp_prixnet", "vsmp_montanttva",
                                                                   "vsmp_lundi", "vsmp_mardi", "vsmp_mercredi", "vsmp_jeudi", "vsmp_vendredi", "vsmp_samedi", "vsmp_coderegate"};
        private static string[] ConnectionFields = new string[] { "record1id", "record2id", "record1roleid", "record2roleid" };
        private static OrganizationServiceProxy _orgService = null;
        private static int NbContracts_Created = 0;
        private static int NbContracts_Updated = 0;
        private static int NB_Errors = 0;
        private static string custId = null;// identifiant client d'un contact donnée 
        private static string telephonemobile = null;//téléphone mobile d'un contact donnée 
        private static string telephonefixe = null;//téléphone fixe d'un contact donnée 
        private static List<Entity> CRMContracts = new List<Entity>();
        private static List<Entity> CRMContacts = new List<Entity>();
        private static List<Entity> CRMConnections = new List<Entity>();
        private static List<Entity> ContratsToCreateOrUpdate = new List<Entity>();
        private static List<Entity> ContactsToCreateOrUpdate = new List<Entity>();
        private static List<Entity> ConnectionsToCreateOrUpdate = new List<Entity>();
        private static List<Entity> CRMConnectionRoles = new List<Entity>();
        private static List<Entity> CRMBusinessUnits = new List<Entity>();
        private static EntityCollection ContractsCollection = new EntityCollection();
        private static EntityCollection ContactsCollection = new EntityCollection();
        private static EntityCollection ConnectionsCollection = new EntityCollection();
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings;
        private static string URL;
        //private static string EndDate = "2018-01-01T00:00:00Z";
        private static string EndDate = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
        private static string startDate;
        #endregion

        #region BatchExecute
        public static void BatchExecute(string[] args)
        {
            try
            {
                //1-Création des logs du jour
                LogHelper.CreateLog();
                //2-connexion CRM
                _orgService = CRMConnection.InitCRMConnection();
                _orgService.Timeout = new TimeSpan(0, 5, 0);
                if (_orgService == null)
                    return;
                LogHelper.WriteHeader("Nouvelle exécution, Debut du traitement...");

                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);
                XmlNode startDateNode = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Contracts_Settings//ContractsStartDate");
                startDate = startDateNode.InnerText;
                URL = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Contracts_Settings//WS_Contracts_URL").InnerText;

                //2-import des contrats
                ContractsImport();           //appel de la fonction d'import des contrat
            }
            catch (Exception ex)
            {
                LogHelper.Writer("Erreur inattendue ! : " + Environment.NewLine + ex.ToString());
                NB_Errors++;
            }
            finally
            {
                LogHelper.WriteHeader("Fin de l'import des contratn nombre d'erreurs : " + NB_Errors );
            }
        } 
        #endregion

        #region Import des contrats (Fonction de Base)
        static void ContractsImport()
        {
            //import des contrats sur la période delta et récupération de ces contrats sur une liste d'objets     
            LogHelper.WriteHeader(string.Format("Exctraction des contrats édités entre : {0} et {1} ...", startDate, EndDate));
            Task<List<Contract>> task = CTR_WS_Connection.GetContracts(URL, startDate, EndDate);
            List<Contract> ListeContrats = task.Result;
            LogHelper.WriteHeader("Fin de l'exctraction des contrats");

            if (ListeContrats != null && ListeContrats.Count != 0 && ListeContrats[0].results != null && ListeContrats[0].results.Length!=0)                                //vérifier si la liste de contrats n'est pas vide (si elle est vide, un objet est comme meme créé avec 0 résultats !)
            {
                foreach (Contract Contrats in ListeContrats)                                                        //parcours des pages de contrats
                {
                    Array.Sort(Contrats.results);                    
                    contrats.AddRange(Contrats.results); ///ajouter a la lise de contrats
                }
                RecordsRetrieve();

                GenerateContacts();
                UpsertRecords(ContactsToCreateOrUpdate, ContactsCollection, "contact");
                CRMContacts = null;        
                CRMContacts = CTR_Application.GetRecords(_orgService, "contact", ContactFields);  //récupérattion de records depuis le CRM pour la suite du traitement

                GenerateContracts();
                UpsertRecords(ContratsToCreateOrUpdate, ContractsCollection, "vsmp_contrat");
                CRMContracts = null;
                CRMContracts = CTR_Application.GetRecords(_orgService, "vsmp_contrat", ContractFields);    //récupérattion de records depuis le CRM pour la suite du traitement

                GenerateConnections();
                UpsertRecords(ConnectionsToCreateOrUpdate, ConnectionsCollection, "connection");

                VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Contracts_Settings//ContractsStartDate").InnerText = EndDate ;
                VSMP_Settings.Save(Xml_Settings_Path);
            }
            else
                LogHelper.Writer("Aucun contrat disponible à importer !");
        }
        #endregion Import des contrats

        #region Récupération des records
        static void RecordsRetrieve()
        {
            LogHelper.WriteHeader("Début de récupération des records de toutes les entités depuis le CRM...");
            CRMContracts = CTR_Application.GetRecords(_orgService, "vsmp_contrat", ContractFields);            //récupération de tous les contrats CRM
            LogHelper.Writer(CRMContracts.Count + " contrats récupérés.");
            CRMContacts = CTR_Application.GetRecords(_orgService, "contact", ContactFields);                   //récupération de tous les contacts CRM
            LogHelper.Writer(CRMContacts.Count + " contacts récupérés.");
            CRMConnections = CTR_Application.GetRecords(_orgService, "connection", ConnectionFields);          //récupération de toutes les connexions CRM
            LogHelper.Writer(CRMConnections.Count + " connexions récupérés.");
            CRMConnectionRoles = CTR_Application.GetRecords(_orgService, "connectionrole", new string[] { "name" });    //récupération de tous les roles de connexions CRM
            CRMBusinessUnits = CTR_Application.GetRecords(_orgService, "businessunit", new string[] { "vsmp_code" });   //récupération de toutess BU CRM
            
            if (CRMContracts == null) CRMContracts = new List<Entity>();
            if (CRMContacts == null) CRMContacts = new List<Entity>();
            if (CRMConnections == null) CRMConnections = new List<Entity>();
            LogHelper.WriteHeader("Fin de récupération des records.");
        }
        #endregion

        #region Génération des contrats
        static void GenerateContracts()
        {
            LogHelper.WriteHeader("Début du traitement des contrats VSMP, génération des records");
            foreach (Result contrat in contrats)    //parcous de la liste de contrats de chaque page
            {
                try
                {
                    //s'il existe une version supérieure de ce contrat dans le CRM, alors ignorer cette version et passer au contrat suivant
                    if (CRMContracts != null && CRMContracts.Any(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber) &&
                        contrat.version < CRMContracts.Find(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber).GetAttributeValue<int>("vsmp_version"))
                        continue;

                    CTR_Application.AssignParameters<Subscriber>(contrat, out custId, out telephonefixe, out telephonemobile);   //Récupération du soucripteur de ce contrats
                    idSubscriber = CTR_Application.GetContact(CRMContacts, custId, telephonefixe, telephonemobile).Id;
                    //vérifier l'existence de ce contrat dans CRM
                    if (CRMContracts != null && CRMContracts.Any(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber))  //si ce contrat existe sur CRM, faire un traitement (mise a jour)
                    {
                        Entity retrievedcontract = CRMContracts.Find(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber);
                        id = retrievedcontract.Id;
                        CRMContracts.Remove(retrievedcontract);
                        if (ContratsToCreateOrUpdate.Any(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber))
                            ContratsToCreateOrUpdate.Remove(ContratsToCreateOrUpdate.Find(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber));
                        //Récupération du canal de validation ET/OU de souscription s'il y a lieu
                        if (contrat.legalInfo.contractStatus == "COMPLETE" && ((OptionSetValue)retrievedcontract["vsmp_statut"]).Value != 688760001)
                        {
                            switch (contrat.system)
                            {
                                case "LPFR":
                                    retrievedcontract.Attributes["vsmp_canalsouscription"] = new OptionSetValue(688760002);
                                    break;
                                case "BP":
                                    retrievedcontract.Attributes["vsmp_canalsouscription"] = new OptionSetValue(688760000);
                                    break;
                                case "CRC":
                                    retrievedcontract.Attributes["vsmp_canalsouscription"] = new OptionSetValue(688760001);
                                    break;
                                case "VSMPTA":
                                    retrievedcontract.Attributes["vsmp_canalsouscription"] = new OptionSetValue(688760003);
                                    break;
                                case "CCU":
                                    retrievedcontract.Attributes["vsmp_canalsouscription"] = new OptionSetValue(688760004);
                                    break;
                                case "LBPIARD":
                                    retrievedcontract.Attributes["vsmp_canalsouscription"] = new OptionSetValue(688760005);
                                    break;
                            }
                        }
                        else if (contrat.legalInfo.contractStatus == "VALIDATED" && ((OptionSetValue)retrievedcontract["vsmp_statut"]).Value != 688760002)
                        {
                            switch (contrat.system)
                            {
                                case "LPFR":
                                    retrievedcontract.Attributes["vsmp_canaldactivation"] = new OptionSetValue(688760002);
                                    break;
                                case "BP":
                                    retrievedcontract.Attributes["vsmp_canaldactivation"] = new OptionSetValue(688760000);
                                    break;
                                case "CRC":
                                    retrievedcontract.Attributes["vsmp_canaldactivation"] = new OptionSetValue(688760001);
                                    break;
                                case "VSMPTA":
                                    retrievedcontract.Attributes["vsmp_canaldactivation"] = new OptionSetValue(688760003);
                                    break;
                                case "CCU":
                                    retrievedcontract.Attributes["vsmp_canaldactivation"] = new OptionSetValue(688760004);
                                    break;
                                case "LBPIARD":
                                    retrievedcontract.Attributes["vsmp_canaldactivation"] = new OptionSetValue(688760005);
                                    break;
                            }
                        }
                        //mettre a jour des champs
                        CTR_Application.MiseAjourContrat(contrat, retrievedcontract, CRMBusinessUnits);
                        retrievedcontract.Attributes["vsmp_souscripteur"] = new EntityReference("contact", idSubscriber);
                        CRMContracts.Add(retrievedcontract);
                        ContratsToCreateOrUpdate.Add(retrievedcontract);
                        NbContracts_Updated++;
                    }
                    else                                                     //Si ce contrat n'existe pas, le créer
                    {
                        Entity newContract = new Entity("vsmp_contrat");
                        switch (contrat.system)
                        {
                            case "BP":
                                newContract.Attributes["vsmp_canaldecreation"] = new OptionSetValue(688760000);
                                break;
                            case "CRC":
                                newContract.Attributes["vsmp_canaldecreation"] = new OptionSetValue(688760001);
                                break;
                            case "LPFR":
                                newContract.Attributes["vsmp_canaldecreation"] = new OptionSetValue(688760002);
                                break;
                            case "VSMPTA":
                                newContract.Attributes["vsmp_canaldecreation"] = new OptionSetValue(688760003);
                                break;
                            case "CCU":
                                newContract.Attributes["vsmp_canaldecreation"] = new OptionSetValue(688760004);
                                break;
                            case "LBPIARD":
                                newContract.Attributes["vsmp_canaldecreation"] = new OptionSetValue(688760005);
                                break;

                        }
                        CTR_Application.RenseignerContrat(newContract, contrat, CRMBusinessUnits);  //renseigner les champs du contrat 
                        newContract.Attributes["vsmp_souscripteur"] = new EntityReference("contact", idSubscriber);
                        CRMContracts.Add(newContract);
                        ContratsToCreateOrUpdate.Add(newContract);
                        NbContracts_Created++;
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Writer("Début Erreur" + Environment.NewLine);
                    LogHelper.Writer("Erreur lors de l'import du Contrat numero :" + contrat.contractNumber + " Version : " + contrat.version + Environment.NewLine + "Type d'erreur : " + ex.ToString() + Environment.NewLine);
                    LogHelper.Writer("Fin Erreur" + Environment.NewLine);
                    NB_Errors++;
                }
            }
            LogHelper.Writer(ContratsToCreateOrUpdate.Count(entity => entity.Id == Guid.Empty).ToString() + " nouveaux Contrats VSMP à créer, "
                             + ContratsToCreateOrUpdate.Count(entity => entity.Id != Guid.Empty).ToString() + " contrats VSMP à mettre à jour.");
            LogHelper.WriteHeader("Fin du traitement des contrats VSMP");
        }
        #endregion

        #region Partie Contacts

        #region Traitement des contacts
        static void GenerateContacts()
        {
            LogHelper.WriteHeader("Début du traitement des contacts, génération des records");
            foreach (Result contrat in contrats)                                                //parcours des contrats d'une page donnée
            {
                try
                {
                    //s'il existe une version supérieure de ce contrat dans le CRM, alors ignorer cette version et passer au contrat suivant
                    if (CRMContracts != null && CRMContracts.Any(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber) &&
                        contrat.version < CRMContracts.Find(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber).GetAttributeValue<int>("vsmp_version"))
                        continue;
                    //Pour les 3 types de contacts, vérifier s'ils existent sur CRM, si oui faire les mises à jour des champs modifiés, sinon les créer 
                    GenererSouscripteur(contrat);
                    GenererBeneficiaire(contrat);
                    GenererP_Prox(contrat);
                }

                catch (Exception ex)
                {
                    LogHelper.Writer("Début Erreur" + Environment.NewLine);
                    LogHelper.Writer("Erreur lors du traitement d'un contact, relié au Contrat numero :" + contrat.contractNumber + " Version : " + contrat.version
                                        + Environment.NewLine + "Type d'erreur : " + ex.ToString() + Environment.NewLine);
                    LogHelper.Writer("Fin Erreur" + Environment.NewLine);
                    NB_Errors++;
                }
            }
            LogHelper.Writer(ContactsToCreateOrUpdate.Count(entity => entity.Id == Guid.Empty).ToString() + " nouveaux Contacts à créer, "
                             + ContactsToCreateOrUpdate.Count(entity => entity.Id != Guid.Empty).ToString() + " contacts à mettre à jour.");
            LogHelper.WriteHeader("Fin du traitement des contacts.");
            
        }
        #endregion

        #region Génération des souscripteurs
        static void GenererSouscripteur(Result contrat)
        {
            //Renseigner les paramètre : customerid, téléphone mobile, téléphone fixe (s'ils existent), et effectuer une vérification d'existence de ce contact basée sur ces infos
            CTR_Application.AssignParameters<Subscriber>(contrat, out custId, out telephonefixe, out telephonemobile);
            //récupérer le souscripteur
            Entity retrievedSubscriber = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile);
            Entity ExistingSubToUpdate = CTR_Application.GetContact(ContactsToCreateOrUpdate, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile);
            if (retrievedSubscriber != null)                 //si ce contact existe alors faire une mise a jour
            {
                idSubscriber = retrievedSubscriber.Id;
                CRMContacts.Remove(retrievedSubscriber);
                if (ExistingSubToUpdate != null)
                    ContactsToCreateOrUpdate.Remove(ExistingSubToUpdate);
                CTR_Application.MettreAjour<Subscriber>(contrat, retrievedSubscriber);
                CRMContacts.Add(retrievedSubscriber);
                ContactsToCreateOrUpdate.Add(retrievedSubscriber);
            }
            else                                            //sinon le créer
            {
                Entity NewContact = new Entity("contact");
                CTR_Application.RenseignerContact<Subscriber>(NewContact, contrat);
                CRMContacts.Add(NewContact);
                ContactsToCreateOrUpdate.Add(NewContact);
            }          
        }
        #endregion 

        #region Génération des Bénéficiaires
        static void GenererBeneficiaire(Result contrat)
        {            
            //Renseigner les paramètre : customerid, téléphone mobile, téléphone fixe (s'ils existent), et effectuer une vérification d'existence de ce contact basée sur ces infos
            CTR_Application.AssignParameters<Recipient>(contrat, out custId, out telephonefixe, out telephonemobile);
            //récupérer le Bénéficiaire
            Entity retrievedRecipient = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile);
            Entity ExistingRecToUpdate = CTR_Application.GetContact(ContactsToCreateOrUpdate, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile);
            if (retrievedRecipient != null)     //si ce contact existe alors faire une mise a jour
            {
                idRecipient = retrievedRecipient.Id;
                CRMContacts.Remove(retrievedRecipient);
                if (ExistingRecToUpdate != null)
                    ContactsToCreateOrUpdate.Remove(ExistingRecToUpdate);
                CTR_Application.MettreAjour<Recipient>(contrat, retrievedRecipient);
                CRMContacts.Add(retrievedRecipient);
                ContactsToCreateOrUpdate.Add(retrievedRecipient);
            }
            else                                  //sinon le créer
            {
                Entity NewContact = new Entity("contact");
                CTR_Application.RenseignerContact<Recipient>(NewContact, contrat);
                CRMContacts.Add(NewContact);
                ContactsToCreateOrUpdate.Add(NewContact);
            }   
        }
        #endregion 

        #region Génération des Personnes de proximité
        static void GenererP_Prox(Result contrat)
        {          
            if (contrat.trustedContact != null && contrat.trustedContact.Length != 0)
            {
                foreach (Trustedcontact trustedcon in contrat.trustedContact) //parcours de la liste de contacts de proximité associés à ce contrat 
                {
                    //Renseigner les paramètre : customerid, téléphone mobile, téléphone fixe (s'ils existent), et effectuer une vérification d'existence de ce contact basée sur ces infos
                    CTR_Application.AssignParameters<Trustedcontact>(contrat, out custId, out telephonefixe, out telephonemobile, trustedcon);
                    //récupération du contact de proximité
                    Entity retrievedTrustContact = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile);
                    Entity ExistingTrustToUpdate = CTR_Application.GetContact(ContactsToCreateOrUpdate, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile);
                    if (retrievedTrustContact != null)  //si ce contact existe sur CRM
                    {
                        //faire une mise a jour du contact de proximité
                        idTrustedContact = retrievedTrustContact.Id;
                        CRMContacts.Remove(retrievedTrustContact);
                        if (ExistingTrustToUpdate != null)
                            ContactsToCreateOrUpdate.Remove(ExistingTrustToUpdate);
                        CTR_Application.MettreAjour<Trustedcontact>(contrat, retrievedTrustContact, trustedcon);
                        CRMContacts.Add(retrievedTrustContact);
                        ContactsToCreateOrUpdate.Add(retrievedTrustContact);

                    }
                    else                               //sinon, le créer
                    {
                        Entity NewContact = new Entity("contact");
                        CTR_Application.RenseignerContact<Trustedcontact>(NewContact, contrat, trustedcon);
                        CRMContacts.Add(NewContact);
                        ContactsToCreateOrUpdate.Add(NewContact);
                    }
                }
            }
        }
        #endregion

        #endregion

        #region Partie Connexions

        #region Génération des connexions
        static void GenerateConnections()
        {        
            LogHelper.WriteHeader("Début du traitement des connexions, génération des records");
            foreach (Result contrat in contrats)
            {
                try
                {
                    //Itérer jusqu'à la dernière version d'un contrat, si la version n'est pas la dernière alors passer au contrat suivant
                    if (!CRMContracts.Any(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber && entity.GetAttributeValue<int>("vsmp_version") == contrat.version))
                        continue;
                    //récupération de cette dernière version de contrat
                    id = CRMContracts.Find(entity => entity.GetAttributeValue<string>("vsmp_name") == contrat.contractNumber).Id;
                    //récupération du souscripteur de ce contrat
                    CTR_Application.AssignParameters<Subscriber>(contrat, out custId, out telephonefixe, out telephonemobile);
                    idSubscriber = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile).Id;
                    //récupération du bénéficiaire de ce contrat
                    CTR_Application.AssignParameters<Recipient>(contrat, out custId, out telephonefixe, out telephonemobile);
                    idRecipient = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile).Id;

                    // Traitement des connexions "Contrat/Souscripteur" , "Contrat/Bénéficiaire" et "Contrat/Personnes de proximité" 

                    Generer_Conx_CTR_SUB(contrat);
                    Generer_Conx_CTR_REC(contrat);
                    Generer_Conx_CTR_TRUST(contrat);

                    // Traitement des connexions entres les 3 types de contacts (Creation Si elle n'existent pas seulement)
                    Generer_Conx_REC_SUB(contrat);
                    Generer_Conx_REC_TRUST(contrat);
                }
                catch (Exception ex)
                {
                    LogHelper.Writer("Début Erreur" + Environment.NewLine);
                    LogHelper.Writer("Erreur lors du traitement d'une connexion, lié au contrat ayant numero :" + contrat.contractNumber + " Version : " + contrat.version + Environment.NewLine + "Type d'erreur : " + ex.ToString() + Environment.NewLine);
                    LogHelper.Writer("Fin Erreur" + Environment.NewLine);
                    NB_Errors++;
                }
            }
            LogHelper.Writer(ConnectionsToCreateOrUpdate.Count(entity => entity.Id == Guid.Empty).ToString() + " nouvelles connexions à créer, "
                             + ConnectionsToCreateOrUpdate.Count(entity => entity.Id != Guid.Empty).ToString() + " connexions à mettre à jour.");
            LogHelper.WriteHeader("Fin du traitement des connexions");        
        }
        #endregion

        #region  Traitement d'une connexion Contrat/Souscripteur
        static void Generer_Conx_CTR_SUB(Result contrat)
        {
            //récupération de la connexion entre ce contrat et son souscripteur
            Entity subconnection = CTR_Application.Retrieve_ContratContact_Connection(id, CTR_Application.RetrieveConnectionRole("Souscripteur", CRMConnectionRoles), CRMConnections);
            Entity ExstSubconnectionToUpdate = CTR_Application.Retrieve_ContratContact_Connection(id, CTR_Application.RetrieveConnectionRole("Souscripteur", CRMConnectionRoles), ConnectionsToCreateOrUpdate);
            if (subconnection != null)               //si cette connexion exste, alors mettre à jour le souscripteur et le lien avec le bénéficiaire
            {
                CRMConnections.Remove(subconnection);
                if (ExstSubconnectionToUpdate != null)
                    ConnectionsToCreateOrUpdate.Remove(ExstSubconnectionToUpdate);
                ContSubConnectionId = subconnection.Id;
                subconnection.Attributes["record2id"] = new EntityReference("contact", idSubscriber);
                switch (contrat.recipient.subscribersRelationship)
                {
                    case "FRIEND":
                        subconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760001);
                        break;
                    case "OTHER":
                        subconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760000);
                        break;
                    case "CHILD":
                        subconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760005);
                        break;
                    case "GRANDCHILD":
                        subconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760002);
                        break;
                    case "ASSOCIATION":
                        subconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760008);
                        break;
                    case "FAMILY_MEMBER":
                        subconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760006);
                        break;
                }
                CRMConnections.Add(subconnection);
                ConnectionsToCreateOrUpdate.Add(subconnection);
            }
            else        //sion créer cette liaison (contrat/souscripteur) et la renseigner
            {
                //création de la connexion entre le contrat et le souscripteur
                Entity newsubconnection = new Entity("connection");
                CTR_Application.Renseigner_ContratContact_Connection<Subscriber>(newsubconnection, contrat, idSubscriber, id, CRMConnectionRoles);
                CRMConnections.Add(newsubconnection);
                ConnectionsToCreateOrUpdate.Add(newsubconnection);
            }
        }
        #endregion

        #region Traitement d'une connexion Contrat/bénéficiaire
        static void Generer_Conx_CTR_REC(Result contrat)
        {
            
            //récupération de la connexion entre ce contrat et son bénéficiaire
            Entity recconnection = CTR_Application.Retrieve_ContratContact_Connection(id, CTR_Application.RetrieveConnectionRole("Bénéficiaire", CRMConnectionRoles), CRMConnections);
            Entity ExstrecconnectionToUpdate = CTR_Application.Retrieve_ContratContact_Connection(id, CTR_Application.RetrieveConnectionRole("Bénéficiaire", CRMConnectionRoles), ConnectionsToCreateOrUpdate);
            if (recconnection != null)         //si cette connexion exste, alors mettre à jour le souscripteur et le lien avec le bénéficiaire
            {
                CRMConnections.Remove(recconnection);
                if (ExstrecconnectionToUpdate != null)
                    ConnectionsToCreateOrUpdate.Remove(ExstrecconnectionToUpdate);
                ContRecConnectionId = recconnection.Id;
                recconnection.Attributes["record2id"] = new EntityReference("contact", idRecipient);
                CRMConnections.Add(recconnection);
                ConnectionsToCreateOrUpdate.Add(recconnection);
            }
            else                              //sion créer cette liaison (contrat/souscripteur) et la renseigner
            {
                //créer une connexion contrat/bénéficiaire
                Entity newrecconnection = new Entity("connection");
                CTR_Application.Renseigner_ContratContact_Connection<Recipient>(newrecconnection, contrat, idRecipient, id, CRMConnectionRoles);
                CRMConnections.Add(newrecconnection);
                ConnectionsToCreateOrUpdate.Add(newrecconnection);
            }
            
        }
        #endregion

        #region Traitement d'une connexion Contrat/P.proximité
        static void Generer_Conx_CTR_TRUST(Result contrat)
        {
            if (contrat.trustedContact != null && contrat.trustedContact.Length != 0)
            {
                foreach (Trustedcontact trustedcon in contrat.trustedContact) //parcours de la liste des contacts de proximité liés à ce contact
                {
                    //récupération du contact préalablement créé depuis le CRM
                    CTR_Application.AssignParameters<Trustedcontact>(contrat, out custId, out telephonefixe, out telephonemobile, trustedcon);
                    idTrustedContact = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile).Id;
                    //recherche de la connexion entre ce contrat et le contact de proximité courant
                    Entity trustconnection = CTR_Application.Retrieve_ContratContact_Connection(id, CTR_Application.RetrieveConnectionRole("Contact de proximité", CRMConnectionRoles),
                                                    CRMConnections, idTrustedContact);
                    Entity ExsttrustconnectionToUpdate = CTR_Application.Retrieve_ContratContact_Connection(id, CTR_Application.RetrieveConnectionRole("Contact de proximité", CRMConnectionRoles),
                                                   ConnectionsToCreateOrUpdate, idTrustedContact);
                    if (trustconnection != null)                     // si cette connexion exite, mettre à jour le lien avec le bénéficiaire
                    {
                        ContTrustConnectionId = trustconnection.Id;
                        CRMConnections.Remove(trustconnection);
                        if (ExsttrustconnectionToUpdate != null)
                            ConnectionsToCreateOrUpdate.Remove(ExsttrustconnectionToUpdate);
                        switch (trustedcon.contactType)
                        {
                            case "NEIGHBOUR":
                                trustconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760003);
                                break;
                            case "FAMILY":
                                trustconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760006);
                                break;
                            case "FRIEND":
                                trustconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760001);
                                break;
                            case "ASSOCIATION_MEMBER":
                                trustconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760007);
                                break;
                            case "OTHER":
                                trustconnection.Attributes["vsmp_lien"] = new OptionSetValue(688760000);
                                break;
                        }
                        CRMConnections.Add(trustconnection);
                        ConnectionsToCreateOrUpdate.Add(trustconnection);
                    }
                    else                                            // sinon, créer cette connexion 
                    {
                        Entity trustedconnection = new Entity("connection");
                        CTR_Application.Renseigner_ContratContact_Connection<Trustedcontact>(trustedconnection, contrat, idTrustedContact, id, CRMConnectionRoles, trustedcon);
                        CRMConnections.Add(trustedconnection);
                        ConnectionsToCreateOrUpdate.Add(trustedconnection);
                    }
                }
            }
            
        }
        #endregion

        #region Traitement d'une connexion bénéficiaire/Souscripteur
        static void Generer_Conx_REC_SUB(Result contrat)
        {          
            if (idRecipient != idSubscriber)        //si le bénéficiaire est différent du souscripteur, alors traiter la connexion entre ces 2 contacts, sinon ne rien faire
            {
                //récupération de la connexion (bénéficiaire/souscripteur)
                Entity recsubconnection = CTR_Application.Retrieve_Contacts_Connection(idRecipient, idSubscriber, CRMConnections);
                if (recsubconnection == null)                               //si cette connexion n'existe pas, alors la créer
                {
                    Entity recsubNewConnection = new Entity("connection");
                    CTR_Application.Renseigner_Contacts_Connection<Subscriber>(recsubNewConnection, contrat, idRecipient, idSubscriber, CRMConnectionRoles);
                    CRMConnections.Add(recsubNewConnection);
                    ConnectionsToCreateOrUpdate.Add(recsubNewConnection);
                }
            }
        }
        #endregion

        #region Traitement d'une connexion bénéficiaire/P.Proximité
        static void Generer_Conx_REC_TRUST(Result contrat)
        {                                    
            if (contrat.trustedContact != null && contrat.trustedContact.Length != 0)   //si la liste des contacts de proximité n'est pas vide, traiter les connexions avec le bénéficiaire
            {
                int i = 0;
                foreach (Trustedcontact trustedcon in contrat.trustedContact)           //parcours de la liste de contacts de proximité
                {
                    //récupération du contact courant depuis la liste des contacts CRM créée
                    CTR_Application.AssignParameters<Trustedcontact>(contrat, out custId, out telephonefixe, out telephonemobile, trustedcon);
                    idTrustedContact = CTR_Application.GetContact(CRMContacts, customerId: custId, telephone: telephonefixe, mobilephone: telephonemobile).Id;
                    if (idTrustedContact != Guid.Empty && idTrustedContact != idRecipient)        //si ce contacts est différent du bénéficiaire, faire un traitement
                    {
                        //recherche d'une connexion entre ces deux contacts
                        Entity recTrustConnection = CTR_Application.Retrieve_Contacts_Connection(idRecipient, idTrustedContact, CRMConnections);
                        if (recTrustConnection == null)         //si elle n'existe pas, alors la créer
                        {
                            Entity rectrustNewConnection = new Entity("connection");
                            CTR_Application.Renseigner_Contacts_Connection<Trustedcontact>(rectrustNewConnection, contrat, idRecipient, idTrustedContact, CRMConnectionRoles, contrat.trustedContact[i]);
                            CRMConnections.Add(rectrustNewConnection);
                            ConnectionsToCreateOrUpdate.Add(rectrustNewConnection);
                        }
                    }
                    i++;
                }
            }
        }
        #endregion

        #endregion 

        #region upsert des records
        static void UpsertRecords(List<Entity> records, EntityCollection collection, string logicalName)
        {
            if (records != null && records.Count != 0)
            {
                foreach (Entity entity in records)
                    collection.Entities.Add(entity);
                collection.EntityName = logicalName;
                //Upsert de records sur CRM
                CTR_Application.CreateMultiple(_orgService, collection);
            }
            else
                LogHelper.Writer("Upsert Impossible : Aucun record trouvé dans la collection d'entités de type : " + logicalName);
        }
        #endregion
    }
}
