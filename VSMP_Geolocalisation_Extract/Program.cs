﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Configuration;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;
using Log;
using CRM_Connection;
using System.Xml;

namespace VSMP_Geolocalisation_Extract
{
    class Program
    {

        #region variables
        private static OrganizationServiceProxy _orgService = null;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings;
        private static string executionDay = DateTime.Today.ToString("yyyyMMdd");
        #endregion variables

        #region Main
        static void Main(string[] args)
        {
            try
            {

                //1-Créaations des logs
                LogHelper.CreateLog();

                //2-connexion CRM et récupération des parametres du fichier de configuration
                _orgService = CRMConnection.InitCRMConnection();
                if (_orgService == null)
                    return;
                LogHelper.WriteHeader("Nouvelle exécution, Debut du traitement... ");

                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);
                string fileName = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Geolocalisation_Extract_Settings//FileName").InnerText;
                string filePath = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Geolocalisation_Extract_Settings/FilePath").InnerText;
                string fullPath = filePath + fileName;

                //3-création de la requete
                #region Querry Creation
                //Creation of query
                QueryExpression querry = new QueryExpression();
                querry.EntityName = "vsmp_contrat";
                ColumnSet cols = new ColumnSet("vsmp_contratid", "vsmp_coderegate", "vsmp_name");
                querry.ColumnSet = cols;
                //querry.AddLink("contact", "vsmp_beneficiaire_id", "contactid");

                //Join with contact (souscripteur)
                LinkEntity le = new LinkEntity("vsmp_contrat", "contact", "vsmp_souscripteur", "contactid", JoinOperator.LeftOuter);
                le.Columns = new ColumnSet("vsmp_identifiantclient", "address1_line3", "address1_postalcode", "address1_city");
                le.EntityAlias = "A";
                querry.LinkEntities.Add(le);

                querry.Criteria.AddCondition("vsmp_coderegate", ConditionOperator.Null);
                #endregion Querry Creation

                //4-parametrage des propiéte de pagination
                int fetchCount = 5000;  // The number of records per page to retrieve.
                int pageNumber = 1;  // Initialize the page number.
                int recordCount = 0; // Initialize the number of records.

                //5-affectation des propiétés de pagination a la requete
                #region Assign pageinfo properties to querry
                // Assign the pageinfo properties to the query expression.
                querry.PageInfo = new PagingInfo();
                querry.PageInfo.Count = fetchCount;
                querry.PageInfo.PageNumber = pageNumber;
                // The current paging cookie. When retrieving the first page, 
                // pagingCookie should be null.
                querry.PageInfo.PagingCookie = null;
                #endregion Assign pageinfo properties to querry

                //6-ecriture des données sous format csv
                #region Write Data to csv

                //reset to page 1
                querry.PageInfo.PageNumber = 1;
                querry.PageInfo.PagingCookie = null;

                //initialize record count
                int reccount = 0;

                // Write sample data to CSV file
                StringBuilder stringb = new StringBuilder();

                //The purpose of this loop if to show number of records in the header
                while (true)
                {
                    EntityCollection collection = _orgService.RetrieveMultiple(querry);
                    foreach (Entity e in collection.Entities)
                    {
                        reccount++;
                    }
                    // Check for more records, if it returns true.
                    if (collection.MoreRecords)
                    {
                        // Increment the page number to retrieve the next page.
                        querry.PageInfo.PageNumber++;
                        // Set the paging cookie to the paging cookie returned from current results.
                        querry.PageInfo.PagingCookie = collection.PagingCookie;
                    }
                    else
                    {
                        // If no more records are in the result nodes, exit the loop.
                        break;
                    }
                }

                //reset to page 1
                querry.PageInfo.PageNumber = 1;
                querry.PageInfo.PagingCookie = null;

                //Header bloc
                string headerLine = "E;" + executionDay + ";" + reccount + ";" + Path.GetFileNameWithoutExtension(fileName);
                stringb.AppendLine(headerLine);


                //detail bloc 
                while (true)
                {

                    //Using retrive multiple from service to get all records
                    EntityCollection csvcollection = _orgService.RetrieveMultiple(querry);

                    if (csvcollection.Entities != null)
                    {

                        //Looping the records
                        foreach (Entity e in csvcollection.Entities)
                        {
                            //defining wanted fields
                            string numcontrat = e.Contains("vsmp_name") ? (e["vsmp_name"]).ToString() : string.Empty;
                            string address1_line3 = e.Contains("A.address1_line3") ? (e["A.address1_line3"] as AliasedValue).Value.ToString() : string.Empty;
                            string address1_postalcode = e.Contains("A.address1_postalcode") ? (e["A.address1_postalcode"] as AliasedValue).Value.ToString() : string.Empty;
                            string address1_city = e.Contains("A.address1_city") ? (e["A.address1_city"] as AliasedValue).Value.ToString() : string.Empty;


                            //writting the records
                            if (e.Contains("vsmp_coderegate") == false)
                            {
                                stringb.Append(
                                    "D" + ";" +
                                    "PZ" + ";" +
                                    numcontrat + ";" +
                                    "" + ";" +
                                    "" + ";" +
                                    "" + ";" +
                                    "" + ";" +
                                    "" + ";" +
                                    "" + ";" +
                                    address1_line3 + ";" +
                                    "" + ";" +
                                    address1_postalcode + ";" +
                                    address1_city);
                            }

                            stringb.AppendLine();

                        }

                    }

                    // Check for more records, if it returns true.
                    if (csvcollection.MoreRecords)
                    {
                        // Increment the page number to retrieve the next page.
                        querry.PageInfo.PageNumber++;
                        // Set the paging cookie to the paging cookie returned from current results.
                        querry.PageInfo.PagingCookie = csvcollection.PagingCookie;
                    }
                    else
                    {
                        // If no more records are in the result nodes, exit the loop.
                        break;
                    }
                }

                //footer bloc
                int linecount = reccount + 2;
                string footerLine = "Z;" + linecount;
                stringb.AppendLine(footerLine);

                //Console.WriteLine();
                LogHelper.Writer("");
                LogHelper.Writer(reccount + " nouveau(x) enregistrement(s) sans code régate trouvé(s)!");

                //convert the string builder to a string
                string convertedString = stringb.ToString();

                //Check if the output directory exists
                if (Directory.Exists(filePath))
                {
                    //stringbuilder to physical csv file
                    using (StreamWriter swriter = new StreamWriter(fullPath))
                    {
                        swriter.Write(stringb.ToString());
                    }
                    //Console.WriteLine("New record(s) sucessfully saved to " + fullpath);
                    //Console.WriteLine();

                    LogHelper.Writer("Les nouveaux enregistrements sont sauvegardés avec succès sur " + fullPath);
                    LogHelper.Writer("");
                }
                else
                {
                    Directory.CreateDirectory(filePath);
                    //stringbuilder to physical csv file
                    using (StreamWriter swriter = new StreamWriter(fullPath))
                    {
                        swriter.Write(stringb.ToString());
                    }
                    Console.WriteLine("The new directory was created");
                    Console.WriteLine("Records sucessfully saved to " + fullPath);
                    Console.WriteLine();
                }
                #endregion Write Data to csv

            }
            catch (Exception ex)
            {
                LogHelper.Writer("### La connexion sur CRM n'a pas abouti, type d'erreur : " + Environment.NewLine + ex.ToString());
                LogHelper.Writer("Erreur: " + ex.Message);
            }
            finally
            {
                //Console.ReadKey();
            }
        }
#endregion Main


    }
}

