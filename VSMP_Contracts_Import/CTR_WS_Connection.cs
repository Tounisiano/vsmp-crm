﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Reflection;
using System.Globalization;
using Log;
using System.Xml;
using System.IO;
using System.Net;

namespace VSMP_Contracts
{
    public static class CTR_WS_Connection
    {
        public async static Task<List<Contract>> GetContracts(string URL, string startDate, string EndDate)
        {
            List<Contract> ListContrats = new List<Contract>();                                 //liste allant contenir des pages de contrats (ou des objets "Contract")           
            int startIndex = 0;
            int NbContracts = 0;
            try
            {
                CustomDelegatingHandler customDelegatingHandler = new CustomDelegatingHandler();
                HttpClient client = HttpClientFactory.Create(customDelegatingHandler);
                
                var watch = System.Diagnostics.Stopwatch.StartNew();

                string urlAppelWS = URL + "&startDate=" + startDate + "&endDate=" + EndDate + "&count=100";

                while (!string.IsNullOrEmpty(urlAppelWS)) //appel du web service en boucle (pour le parcours de la pagination) jusqu'à ce que l'on ait plus de page suivante
                {
                    
                    HttpResponseMessage response = await client.GetAsync(urlAppelWS);
                   

                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception();

                    string responsedata = await response.Content.ReadAsStringAsync();
                  
                    Contract Contrats = JsonConvert.DeserializeObject<Contract>(responsedata);
                    if(Contrats.results.Length == 0)
                    {
                        break;
                    }
                    ListContrats.Add(Contrats);        //ajout de la réponse en page de contrats dans la liste
                    urlAppelWS = string.IsNullOrEmpty(Contrats.nextResults) ? string.Empty : URL + Contrats.nextResults.Split('?')[1];
                    Console.WriteLine("Nouvelle page de contrats : {0} contrats", Contrats.results.Length);
                    NbContracts += Contrats.results.Length;
                }
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                LogHelper.Writer(string.Format("Temps d'extraction des contrats depuis le web service : {0} ms soit {1} s", elapsedMs, elapsedMs / 1000));

                if (NbContracts != 0)
                    LogHelper.Writer(string.Format("{0} contrats extrais répartis sur {1} pages", NbContracts, startIndex));
            }
            catch (NullReferenceException ex)
            {
                LogHelper.Writer("Erreur (référence nulle) lors de l'import des contrats depuis le web service, message de l'erreur : " + Environment.NewLine + ex.ToString());
                throw ex;
            }
            catch (Exception ex)
            {
                LogHelper.Writer("Erreur lors de l'import des contrats depuis le web service, message de l'erreur : " + ListContrats.Count + Environment.NewLine + ex.ToString());
                throw ex;
            }
            return ListContrats;
        }

        public class CustomDelegatingHandler : DelegatingHandler
        {
            //Obtained from the server earlier, APIKey MUST be stored securely and in App.Config
            private string _applicationId;
            private string _secretKey;
            private string _proxyurl;
            private int _proxyport;
            private string _proxylogin;
            private string _proxypass;
            private bool _proxyswitch;
            private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
            private static XmlDocument VSMP_Settings;

            public static string UpperCaseUrlEncode(string s)
            {
                char[] temp = HttpUtility.UrlEncode(s).ToCharArray();
                for (int i = 0; i < temp.Length - 2; i++)
                {
                    if (temp[i] == '%')
                    {
                        temp[i + 1] = char.ToUpper(temp[i + 1]);
                        temp[i + 2] = char.ToUpper(temp[i + 2]);
                    }
                }
                return new string(temp);
            }

            protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {

                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);

                _proxyurl = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Proxy_Settings//ProxyUrl").InnerText;
                _proxyport = Convert.ToInt32(VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Proxy_Settings//ProxyPort").InnerText);
                _proxylogin = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Proxy_Settings//ProxyLogin").InnerText;
                _proxypass = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Proxy_Settings//ProxyPass").InnerText;
                _proxyswitch = Convert.ToBoolean(VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Proxy_Settings//ProxySwitch").InnerText);

                //proxy bypass connection settings
                if (_proxyswitch)
                {
                    var proxy = new WebProxy(_proxyurl, _proxyport);
                    proxy.Credentials = new NetworkCredential(_proxylogin, _proxypass);
                    WebRequest.DefaultWebProxy = proxy;
                }
           
                HttpResponseMessage response = null;
              
                _applicationId = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Contracts_Settings//HmacID").InnerText;
                _secretKey = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Contracts_Settings//HmacSecret").InnerText;
                
                var _secretKeyUTF8 = UTF8Encoding.UTF8.GetBytes(_secretKey);
                string nonce = Guid.NewGuid().ToString("N");

                var gbCulture = new CultureInfo("en-GB");
                // Get current UTC time.   
                var utcDate = DateTime.UtcNow;

                var utcFormattedDate = utcDate.ToString("ddd, dd MMM yyyy HH:mm:ss UTC", gbCulture);

                //string Date = "date:" + UpperCaseUrlEncode(str);
                string Date = "date: " + utcFormattedDate;
                //Date = "date:Thu%2C+21+Apr+2016+13%3A32%3A18+UTC";
                using (HMACSHA1 hmac = new HMACSHA1(_secretKeyUTF8))
                {
                    byte[] Datebytes = Encoding.UTF8.GetBytes(Date);
                    byte[] hashedValue = hmac.ComputeHash(Datebytes);
                    string Signature = Convert.ToBase64String(hashedValue);
                    string Url_Encoded_Signature = UpperCaseUrlEncode(Signature);

                    request.Headers.Add("Authorization", string.Format("Signature keyId=\"{0}\",algorithm=\"{1}\",signature=\"{2}\"", _applicationId, "hmac-sha1", Url_Encoded_Signature));

                    var invalidHeaders = (HashSet<string>)typeof(HttpHeaders).GetField("invalidHeaders", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(request.Headers);
                    invalidHeaders.Remove("Content-Type");
                    request.Headers.Remove("Content-Type");
                    request.Headers.Add("Content-Type", "application/json;charset=UTF-8");
                    request.Headers.TryAddWithoutValidation("Date", utcFormattedDate);                     
                }
                response = await base.SendAsync(request, cancellationToken);
                return response;
            }
        }
    }
}
