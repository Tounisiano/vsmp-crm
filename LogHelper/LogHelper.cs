﻿using System;
using System.IO;

namespace Log
{
    public class LogHelper
    {
        private static StreamWriter writer;
        private static string fullFilename;
        private static DateTime nowDate = DateTime.Now;

        public static void CreateLog()
        {
            try
            {
                string shortDate = String.Format("{0:yyyy-MM-dd}", nowDate);
                string filename = string.Format("{0}.log", shortDate);
                //création du nom de fichier .log
                filename = filename.Replace("/", "-");
                //récupérer le path complet, application PC
                string rootPath = Path.GetFullPath("./Data/Log/");
                //ou le path pour une application serveur
                //System.Web.HttpContext.Current.Server.MapPath("~/Data/Log/");
                fullFilename = string.Format(@"{0}{1}", rootPath, filename);
                //vérifier les dossiers Data & Log
                if (!Directory.Exists(rootPath))
                {
                    //création du dossier
                    Directory.CreateDirectory(rootPath);
                }
                //vérifier le fichier
                if (!File.Exists(fullFilename))
                {
                    //création du fichier log du jour
                    FileStream f = File.Create(fullFilename);
                    f.Close();
                }
                using(writer = new StreamWriter(fullFilename, true))
                    writer.WriteLine("________________________________________________________________________________________________________________________________________________________________________" +
                      Environment.NewLine + "///////////////////////////////////////////////////////////////////////////////// LOG FILE /////////////////////////////////////////////////////////////////////////////" +
                      Environment.NewLine + "________________________________________________________________________________________________________________________________________________________________________");
            }                               
            catch (Exception ex)
            {
                throw new Exception("Erreur lors de la création du fichier log", ex);
            }
        }
        public static void Writer(string Message)
        {
            using (writer = new StreamWriter(fullFilename, true))
            {
                writer.WriteLine(DateTime.Now + "   |   " + Message);
            }
        }
        public static void WriteHeader(string Message)
        {
            using (writer = new StreamWriter(fullFilename, true))
            {
                writer.WriteLine(DateTime.Now + "   |   ------------------------------" + Message + "-------------------------------");
                writer.WriteLine(Environment.NewLine);
            }
        }
        //2fichiers log erreur et fonctionnel      
    }
}
