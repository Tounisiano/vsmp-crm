﻿using System.Collections.Generic;
using System.Linq;
using CsvHelper;
using System.IO;
using CsvHelper.Configuration;
using System;

namespace VSMP_Organisation
{
    public static class ORG_Fields_Mapping
    {
        public static List<Organisation> MapData(string path) //le path va devenir le string retourné par le WS
        {
            List<Organisation> results;
            using (TextReader sr = new StringReader(path))
            using (var reader = new CsvReader(sr))
            {
                reader.Configuration.Delimiter = ";";
                reader.Configuration.HasHeaderRecord = true;
                reader.Configuration.RegisterClassMap<OrganisationDataMap>();
                results = reader.GetRecords<Organisation>().ToList();
                results = results.Where(organisation => organisation.LIBELLE_COURT_TYPE == "BC" || organisation.LIBELLE_COURT_TYPE == "BP" || 
                                        organisation.LIBELLE_COURT_TYPE == "DR" || organisation.LIBELLE_COURT_TYPE == "DEX" || organisation.LIBELLE_COURT_TYPE == "ZM").ToList();
                results = results.Where(organisation => organisation.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT == "BC" || organisation.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT == "BP" ||
                                        organisation.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT == "DR" || organisation.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT == "DEX" ||
                                        organisation.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT == "ZM").ToList(); //instruction a confirmer
                results.Sort();
                return results;
            }
        }
    }
    class OrganisationDataMap : CsvClassMap<Organisation>
    {
        public OrganisationDataMap() //on choisit ici quoi mapper comme données
        {   
            //AutoMap (utiliser cette fct pour faire un proable mapping automatique)
            Map(x => x.CODE_REGATE);
            Map(x => x.LIBELLE);
            Map(x => x.LIBELLE_COURT_TYPE);
            Map(x => x.CODE_REGATE_ENTITE_RATTACHEMENT);
            Map(x => x.LIBELLE_ENTITE_RATTACHEMENT);
            Map(x => x.LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT);
        }
    }
    
}
