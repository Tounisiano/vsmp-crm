﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Configuration;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;
using System.Xml;
using Log;
using CRM_Connection;

namespace VSMP_Geolocalisation_Import
{
    class Program
    {

        #region variables
        private static OrganizationServiceProxy _orgService = null;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings;
        private static string executionDay = DateTime.Today.ToString("yyyyMMdd");
        #endregion variables

        static void Main(string[] args)
        {

            try
            {   

                //1-Créaations des logs
                LogHelper.CreateLog();

                //2-connexion CRM et récupération des parametres du fichier de configuration
                _orgService = CRMConnection.InitCRMConnection();
                if (_orgService == null)
                    return;
                LogHelper.WriteHeader("Nouvelle exécution, Debut du traitement... ");

                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);
                string fileName = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Geolocalisation_Import_Settings//FileName").InnerText;
                string filePath = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Geolocalisation_Import_Settings/FilePath").InnerText;
                string fullPath = filePath + fileName;

                //3-création de la requete
                #region Querry Creation
                //Creation of query
                QueryExpression querry = new QueryExpression();
                querry.EntityName = "vsmp_contrat";
                ColumnSet cols = new ColumnSet("vsmp_contratid", "vsmp_coderegate", "vsmp_name");
                querry.ColumnSet = cols;
                //querry.AddLink("contact", "vsmp_beneficiaire_id", "contactid");

                //Join with contact (souscripteur)
                LinkEntity le = new LinkEntity("vsmp_contrat", "contact", "vsmp_souscripteur", "contactid", JoinOperator.LeftOuter);
                le.Columns = new ColumnSet("vsmp_identifiantclient", "address1_line3", "address1_postalcode", "address1_city");
                le.EntityAlias = "A";
                querry.LinkEntities.Add(le);

                querry.Criteria.AddCondition("vsmp_coderegate", ConditionOperator.Null);
                #endregion Querry Creation

                //4-parametrage des propiéte de pagination
                int fetchCount = 5000;  // The number of records per page to retrieve.
                int pageNumber = 1;  // Initialize the page number.
                int recordCount = 0; // Initialize the number of records.

                //5-affectation des propiétés de pagination a la requete
                #region Assign pageinfo properties to querry
                // Assign the pageinfo properties to the query expression.
                querry.PageInfo = new PagingInfo();
                querry.PageInfo.Count = fetchCount;
                querry.PageInfo.PageNumber = pageNumber;
                // The current paging cookie. When retrieving the first page, 
                // pagingCookie should be null.
                querry.PageInfo.PagingCookie = null;
                #endregion Assign pageinfo properties to querry

                EntityCollection mycollection = _orgService.RetrieveMultiple(querry);
                List<Entity> myentities = new List<Entity>();

                foreach (Entity entity in mycollection.Entities)
                {


                    myentities.Add(entity);
                }

                #region read from csv

                List<string> listA = new List<string>();

                //testing if path exists
                if ((File.Exists(fullPath)))
                {

                    //Console.WriteLine("Reading from file...");
                    //Console.WriteLine("The following records are going to be updated...");
                    LogHelper.Writer("Les enregistrements suivants seront mis à jour...");
                    Console.WriteLine();

                    //looping to read csv file line by line
                    using (var reader = new StreamReader(@fullPath))
                    {
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            listA.Add(line);
                        }
                    }

                    //listA.ForEach(Console.WriteLine);
                    foreach (var entity in listA)
                    {
                        LogHelper.Writer(entity);
                    }


                }
                else
                {

                    LogHelper.Writer("Le fichier n'existe pas. Un nouveau fichier a été crée.");
                }

                #endregion read from csv

                #region Update new codereg
                int numofupdates = 0;

                //reset to page 1
                querry.PageInfo.PageNumber = 1;
                querry.PageInfo.PagingCookie = null;

                //Defining two lists 
                List<string> codes = new List<string>();
                List<string> ids = new List<string>();

                //Defining a new entity collection to store the to be updated entities
                EntityCollection tobeupdated = new EntityCollection();



                //filling the list of codes from array of codes --convert to list--
                for (int i = 1; i < listA.Count - 1; i++)
                {
                    var values = listA[i].Split(';');
                    string coderegate = values[5];
                    string numcontrat = values[2];
                    codes.Add(coderegate);
                    ids.Add(numcontrat);



                }


                for (int i = 0; i < ids.Count; i++)
                {

                    Entity updatedcontract = myentities.Find(entity => entity.GetAttributeValue<string>("vsmp_name") == ids.ElementAt(i));

                    if (updatedcontract != null)
                    {
                        //assign the new code regate to its crm field
                        updatedcontract["vsmp_coderegate"] = codes.ElementAt(i);

                        //Add the to be updated entity to the collection
                        tobeupdated.Entities.Add(updatedcontract);

                        //incrementing number of records to update
                        ++numofupdates;

                    }
                    else
                    {
                        LogHelper.Writer("ERROR");
                    }

                }







                try
                {
                    // Execute BulkUpdate on the tobeupdated entity colection. 
                    BulkUpdate(_orgService, tobeupdated);
                    //Console.WriteLine();
                    //Console.WriteLine( numofupdates + " records successfully updated!");
                    LogHelper.Writer("");
                    LogHelper.Writer(numofupdates + " enregistrement(s) mis à jour!");

                }
                // Catch any service fault exceptions that Microsoft Dynamics CRM throws.
                catch (FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> ex)
                {
                    LogHelper.Writer("error: " + ex.Message);
                }
                catch (Exception ex)
                {
                    LogHelper.Writer("error: " + ex.Message);
                }
                #endregion Update new codereg


             }
            catch (Exception ex)
            {
                LogHelper.Writer("### La connexion sur CRM n'a pas abouti, type d'erreur : " + Environment.NewLine + ex.ToString());
                
                LogHelper.Writer("Error: " + ex.Message);
            }




        }

        #region bulk update
        /// <summary>
        /// Call this method for bulk update
        /// </summary>
        /// <param name="service">Org Service</param>
        /// <param name="entities">Collection of entities to Update</param>
        public static void BulkUpdate(IOrganizationService service, EntityCollection entities)
        {
            // Create an ExecuteMultipleRequest object.
            var multipleRequest = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            // Add a UpdateRequest for each entity to the request collection.
            foreach (var entity in entities.Entities)
            {
                UpdateRequest updateRequest = new UpdateRequest { Target = entity };
                multipleRequest.Requests.Add(updateRequest);
            }

            // Execute all the requests in the request collection using a single web method call.
            ExecuteMultipleResponse multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequest);

        }
        #endregion bulk update
    }
}
