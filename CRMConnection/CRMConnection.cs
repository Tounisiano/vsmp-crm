﻿using System;
using System.Net;
using Microsoft.Xrm.Sdk.Client;
using System.Reflection;
using System.ServiceModel.Description;
using System.IO;
using System.Xml;
using System.Text;
using Log;
using System.Security.Authentication;

namespace CRM_Connection
{
    public static class CRMConnection
    {
        private static OrganizationServiceProxy sp = null;
        private static IWebProxy proxy = WebRequest.DefaultWebProxy;
        private static string URL;
        private static string Login;
        private static string MDP;
        private static string B64_Encoded_MDP;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings;

        public static OrganizationServiceProxy InitCRMConnection()
        {
            try
            {


                const SslProtocols _Tls12 = (SslProtocols)0x00000C00;

                const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;

                ServicePointManager.SecurityProtocol = Tls12;

                // Récupération des paramètres de connexion
                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);
                B64_Encoded_MDP = VSMP_Settings.DocumentElement.SelectSingleNode("//CRMConnection_Settings//MDP").InnerText;
                MDP = Base64Decode(B64_Encoded_MDP);
                URL = VSMP_Settings.DocumentElement.SelectSingleNode("//CRMConnection_Settings//CRMLink").InnerText;
                Login = VSMP_Settings.DocumentElement.SelectSingleNode("//CRMConnection_Settings//Login").InnerText;

                PropertyInfo _WebProxy = proxy.GetType().GetProperty("WebProxy", BindingFlags.NonPublic | BindingFlags.Instance);
                WebProxy wProxy = (WebProxy)_WebProxy.GetValue(proxy, null);
                wProxy.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                

                // Les paramètres de connexion doivent tous être renseignés
                if (string.IsNullOrEmpty(URL) || string.IsNullOrEmpty(Login) || string.IsNullOrEmpty(MDP))
                    throw new Exception("La connexion avec l'intranet n'a pas pu être établie. Veuillez vérifier les paramètres de connexion dans le fichier de configuration du batch");

                Uri OrganizationUri = new Uri(Path.Combine(URL + "XRMServices/2011/Organization.svc"));
                ClientCredentials cc = new ClientCredentials();
                ClientCredentials cd = null;

                cc.UserName.UserName = Login;
                cc.UserName.Password = MDP;

                // On se connecte
                sp = new OrganizationServiceProxy(OrganizationUri, null, cc, cd);
                sp.ServiceConfiguration.CurrentServiceEndpoint.EndpointBehaviors.Add(new ProxyTypesBehavior());

            }
            catch (Exception ex)
            {
                LogHelper.Writer("Erreur lors de la connexion au crm, Message de l'erreur : " + Environment.NewLine + ex.ToString());
            }
            return sp;
        }       
        //fonction de décodage de la clé
        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
