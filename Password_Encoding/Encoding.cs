﻿using System;
using System.IO;
using System.Text;
using System.Xml;

namespace Password_Encoding
{
    class Password_Encoding
    {
        private static string NewPassword;
        private static string Encoded_NewPassword;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings_File;

        static void Main(string[] args)
        {
            Console.WriteLine("Entrer le nouveau mot de passe :");
            try
            { 
                NewPassword = Console.ReadLine();
                Encoded_NewPassword = Base64Encode(NewPassword);
                Console.WriteLine("Encodage réussi, voici la clé encodée : " + Encoded_NewPassword + Environment.NewLine);
                Console.WriteLine("Enregistrement de la clé dans le fichier de configuration..." + Environment.NewLine);
                VSMP_Settings_File = new XmlDocument();
                VSMP_Settings_File.Load(Xml_Settings_Path);
                VSMP_Settings_File.DocumentElement.SelectSingleNode("//CRMConnection_Settings//MDP").InnerText = Encoded_NewPassword;                              
                VSMP_Settings_File.Save(Xml_Settings_Path);                Console.WriteLine("Fin de l'enregistrement. Cliquez pour terminer..");                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Erreur lors du transformation du mot de passe." + Environment.NewLine + "Type d'erreur : " + ex.ToString());
            }
            finally
            {
                Console.ReadKey();
            }
        }
        //fonction d'encodage de la clé
        private static string Base64Encode(string plainText)
        {           
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes); 
        }
    }
}
