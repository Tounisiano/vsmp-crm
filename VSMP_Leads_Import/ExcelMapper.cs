﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;


namespace VSMP_Leads
{
    public static class ExcelMapper
    {
        private static List<string> ExcelFields = new List<string>() { "(Ne pas modifier) Prospect", "(Ne pas modifier) Somme de contrôle de la ligne", "(Ne pas modifier) Modifié le", "Prénom", "Nom de famille",
                "Mail", "Téléphone professionnel", "Téléphone mobile", "Adresse 1 : Rue 1", "Adresse 1 : Rue 2", "Adresse 1 : Rue 3", "Adresse 1 : Rue 4", "Adresse 1 : Ville", "Adresse 1 : Code postal", "Commentaire", "Statut apport",
                "Motif Abandonné", "Type VSMP", "Ref apport", "Date", "Ouvert", "Canal", "Type de prospect", "Statut du contrat", "Numéro du contrat", "Veille", "Type apporteur", "Nom de l'apporteur",
                "Prénom de l'apporteur", "IDRH", "Code Régate", "Type contact lié", "Nom du contact 2", "Prénom du contact 2", "Type contact lié 2", "Téléphone mobile 2", "Téléphone professionnel 2",
                "Courrier électronique 2", "Adresse2:Rue1", "Adresse2:Rue2", "Adresse2:Rue3", "Adresse 2 : Rue 4", "Adresse2:Code postal", "Adresse2:Ville", "Nom du contact 3",
                "Prénom du contact 3", "Type contact lié 3", "Téléphone mobile 3", "Téléphone professionnel 3", "Courrier électronique 3", "Adresse 3 : Rue 1", "Adresse 3 : Rue 2", "Adresse3:Rue3", "Adresse 3 : Rue 4",
                "Adresse 3 : Code postal", "Adresse 3 : Ville", "Action", "Date limite", "Statut de l'action", "Contact", "Commentaire interne CRC"};

        public static List<Lead> MapData(string dirPath)
        {            
            List<Lead> Results = new List<Lead>();
            PropertyInfo[] LeadProperties = typeof(Lead).GetProperties();
            try
            {
                ExcelPackage xlPackage = new ExcelPackage(new FileInfo(dirPath));
                var myWorksheet = xlPackage.Workbook.Worksheets.First(); //select sheet here
                var totalRows = myWorksheet.Dimension.End.Row;
                var totalColumns = myWorksheet.Dimension.End.Column;

                int DateIndex = GetIndex(myWorksheet, totalColumns, ExcelFields.FindIndex(x => x == "Date"));
                int OuvertIndex = GetIndex(myWorksheet, totalColumns, ExcelFields.FindIndex(x => x == "Ouvert"));
                int DateLimiteIndex = GetIndex(myWorksheet, totalColumns, ExcelFields.FindIndex(x => x == "Date limite"));

                var Dates = myWorksheet.Cells.Where(col => col.Text == "Date" || col.Text == "Ouvert" || col.Text == "Date limite");

                myWorksheet.Column(DateIndex).Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                myWorksheet.Column(OuvertIndex).Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                myWorksheet.Column(DateLimiteIndex).Style.Numberformat.Format = DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
                xlPackage.Save();

                for (int j = 2; j < totalRows + 1; j++)
                {
                    Lead lead = new Lead();
                    for (int i = 0; i < LeadProperties.Length; i++)
                    {
                        var value = myWorksheet.Cells[j, GetIndex(myWorksheet, totalColumns, i)].Text;
                        LeadProperties[i].SetValue(lead, value);
                    }
                    Results.Add(lead);
                }
                return Results;
            }
            catch (Exception ex)
            {
                throw new Exception("Error lors du mapping du fichier d'apports !", ex);
            }
        }
        static int GetIndex (ExcelWorksheet myWorksheet, int totalcolumns, int fieldindex) //recherche d'un indice d'une propriété indexée (par fieldindex)
        {
            int index=1;
            for(int i = 1;  i < totalcolumns + 1;  i++)
            {
                if (myWorksheet.Cells[1, i].Text == ExcelFields[fieldindex])
                {
                    index = i;
                    break;
                }
            }
            return index;
        }
    }

}
