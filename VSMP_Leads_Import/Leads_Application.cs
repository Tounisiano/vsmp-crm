﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using Log;
using Microsoft.Xrm.Sdk.Client;
using System.Globalization;

namespace VSMP_Leads
{
    class Leads_Application
    {
        #region Récupération de tous les records d'une entité donnée depuis le CRM
        public static List<Entity> GetRecords(IOrganizationService _orgService, string EntityLogicalName, string[] CRMFields)
        {            
            List<EntityCollection> recordsRetrieved = new List<EntityCollection>();
            int querycount = 5000;
            int pagenumber = 1;
            List<Entity> CRMRecords = new List<Entity>();
            QueryExpression query = new QueryExpression(EntityLogicalName)
            { ColumnSet = new ColumnSet(CRMFields) };
            query.PageInfo = new PagingInfo();
            query.PageInfo.Count = querycount;
            query.PageInfo.PageNumber = pagenumber;
            query.PageInfo.PagingCookie = null;
           
            while (true)
            {
                EntityCollection newCollection = _orgService.RetrieveMultiple(query);
                recordsRetrieved.Add(newCollection);
                if (newCollection.MoreRecords)
                {
                    query.PageInfo.PageNumber++;
                    query.PageInfo.PagingCookie = newCollection.PagingCookie;
                }
                else
                    break;
            }
            if (recordsRetrieved == null || recordsRetrieved.Count < 1 || recordsRetrieved[0].Entities == null)
                return null;
            else
            {
                foreach (EntityCollection collection in recordsRetrieved)
                {
                    foreach (Entity entity in collection.Entities)
                        CRMRecords.Add(entity);
                }
                return CRMRecords;
            }
        }
        #endregion

        #region Renseigner un apport
        public static void RenseignerLead(Entity Newlead, Lead lead, List<Entity> CRMBusinessUnits)
        {
                      
            if(lead.Date!=string.Empty && lead.Date!= "NULL")
                Newlead.Attributes["vsmp_date"] = DateTime.Parse(lead.Date);

            if(GetBU(lead.Code_Régate, CRMBusinessUnits) != Guid.Empty)
                Newlead.Attributes["vsmp_coderegate"] = lead.Code_Régate;

            if (lead.Date_limite != string.Empty && lead.Date_limite != "NULL")
                Newlead.Attributes["estimatedclosedate"] = DateTime.Parse(lead.Date_limite);

            if (lead.Ouvert != string.Empty && lead.Ouvert != "NULL")
                Newlead.Attributes["vsmp_datedouverture"] = DateTime.Parse(lead.Ouvert);

            switch (lead.Canal) 
            {
                case "VSMP":
                    Newlead.Attributes["vsmp_canal"] = new OptionSetValue(688760000);
                    break;
                case "ETB":
                    Newlead.Attributes["vsmp_canal"] = new OptionSetValue(688760001);
                    break;
                case "BP":
                    Newlead.Attributes["vsmp_canal"] = new OptionSetValue(688760002);
                    break;
                case "RE":
                    Newlead.Attributes["vsmp_canal"] = new OptionSetValue(688760003);
                    break;
                default:
                    break;
            }
            switch (lead.Type_de_prospect)
            {
                case "Postier":
                    Newlead.Attributes["vsmp_typedeprospect"] = new OptionSetValue(688760000);
                    break;
                case "Classique":
                    Newlead.Attributes["vsmp_typedeprospect"] = new OptionSetValue(688760001);
                    break;
                case "VIP":
                    Newlead.Attributes["vsmp_typedeprospect"] = new OptionSetValue(688760002);
                    break;
                case "VSMP":
                    Newlead.Attributes["vsmp_typedeprospect"] = new OptionSetValue(100000000);
                    break;
                default:
                    break;
            }
            switch (lead.Statut_du_contrat)
            {
                case "Créé":
                    Newlead.Attributes["vsmp_statutducontrat"] = new OptionSetValue(688760000);
                    break;
                case "Souscrit":
                    Newlead.Attributes["vsmp_statutducontrat"] = new OptionSetValue(688760001);
                    break;
                case "Activé":
                    Newlead.Attributes["vsmp_statutducontrat"] = new OptionSetValue(688760002);
                    break;
                case "En création":
                    Newlead.Attributes["vsmp_statutducontrat"] = new OptionSetValue(100000000);
                    break;
                default:
                    break;
            }
            switch (lead.Type_contact_lié)
            {
                case "Souscripteur":
                    Newlead.Attributes["vsmp_typecontact"] = new OptionSetValue(688760000);
                    break;
                case "Bénéficiaire":
                    Newlead.Attributes["vsmp_typecontact"] = new OptionSetValue(688760001);
                    break;
                case "Proche du bénéficiaire":
                    Newlead.Attributes["vsmp_typecontact"] = new OptionSetValue(688760002);
                    break;
                default:
                    break;
            }
            switch (lead.Type_contact_lié_2)
            {
                case "Souscripteur":
                    Newlead.Attributes["vsmp_typecontact2"] = new OptionSetValue(688760000);
                    break;
                case "Bénéficiaire":
                    Newlead.Attributes["vsmp_typecontact2"] = new OptionSetValue(688760001);
                    break;
                case "Proche du bénéficiaire":
                    Newlead.Attributes["vsmp_typecontact2"] = new OptionSetValue(688760002);
                    break;
                default:
                    break;
            }
            switch (lead.Type_contact_lié_3)
            {
                case "Souscripteur":
                    Newlead.Attributes["vsmp_typecontact3"] = new OptionSetValue(688760000);
                    break;
                case "Bénéficiaire":
                    Newlead.Attributes["vsmp_typecontact3"] = new OptionSetValue(688760001);
                    break;
                case "Proche du bénéficiaire":
                    Newlead.Attributes["vsmp_typecontact3"] = new OptionSetValue(688760002);
                    break;
                default:
                    break;
            }
            switch (lead.Type_apporteur)
            {
                case "Facteur":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760000);
                    break;
                case "CharCli":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760001);
                    break;
                case "CoBA":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760002);
                    break;
                case "VSMP":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760003);
                    break;
                case "Autre":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760004);
                    break;
                case "CRC":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760005);
                    break;
                case "Partenaire":
                    Newlead.Attributes["vsmp_typeapporteur"] = new OptionSetValue(688760006);
                    break;
                default:
                    break;
            }
            switch (lead.Action)
            {
                case "Attente client":
                    Newlead.Attributes["vsmp_action"] = new OptionSetValue(688760000);
                    break;
                case "Contacter autre personne":
                    Newlead.Attributes["vsmp_action"] = new OptionSetValue(688760001);
                    break;
                case "Envoyer un document":
                    Newlead.Attributes["vsmp_action"] = new OptionSetValue(688760002);
                    break;
                case "Rappeler":
                    Newlead.Attributes["vsmp_action"] = new OptionSetValue(688760003);
                    break;
                default:
                    break;
            }
            switch (lead.Statut_de_laction)
            {
                case "A Faire":
                    Newlead.Attributes["vsmp_statutdelaction"] = false;
                    break;
                case "Fait":
                    Newlead.Attributes["vsmp_statutdelaction"] = true;
                    break;
                default:
                    break;
                    //gérer le cas null sur les optionSet et booléens
            }
            switch (lead.Statut_apport)
            {
                case "Activé":
                    Newlead.Attributes["vsmp_statutprospect"] = new OptionSetValue(688760000);
                    break;
                case "En traitement":
                    Newlead.Attributes["vsmp_statutprospect"] = new OptionSetValue(688760001);
                    break;
                case "Souscrit":
                    Newlead.Attributes["vsmp_statutprospect"] = new OptionSetValue(688760002);
                    break;
                case "Abandonné":
                    Newlead.Attributes["vsmp_statutprospect"] = new OptionSetValue(688760003);
                    break;
                default:
                    break;
            }
            switch (lead.Motif_Abandonné)
            {
                case "Refus d'être contacté":
                    Newlead.Attributes["vsmp_motifdabandon"] = new OptionSetValue(688760000);
                    break;
                case "Offre inappropriée":
                    Newlead.Attributes["vsmp_motifdabandon"] = new OptionSetValue(688760001);
                    break;
                case "Prix élevé":
                    Newlead.Attributes["vsmp_motifdabandon"] = new OptionSetValue(688760002);
                    break;
                case "Refus parent":
                    Newlead.Attributes["vsmp_motifdabandon"] = new OptionSetValue(688760003);
                    break;
                case "Autre":
                    Newlead.Attributes["vsmp_motifdabandon"] = new OptionSetValue(688760004);
                    break;
                default:
                    break;
            }
            switch (lead.Type_VSMP)
            {
                case "Apport ADA":
                    Newlead.Attributes["vsmp_typevsmp"] = new OptionSetValue(688760000);
                    break;
                case "Liste marketing":
                    Newlead.Attributes["vsmp_typevsmp"] = new OptionSetValue(688760001);
                    break;
            }
            
            if(lead.Ref_apport != string.Empty)
                Newlead.Attributes["vsmp_refapport"] = lead.Ref_apport;
            if (lead.Numéro_du_contrat != string.Empty)
                Newlead.Attributes["vsmp_numeroducontrat"] = lead.Numéro_du_contrat;
            if (lead.Veille != string.Empty)
                Newlead.Attributes["vsmp_veille"] = lead.Veille;
            if (lead.Prénom != string.Empty)
                Newlead.Attributes["firstname"] = lead.Prénom;
            if (lead.Nom_de_famille != string.Empty)
                Newlead.Attributes["lastname"] = lead.Nom_de_famille;
            if (lead.Téléphone_mobile != string.Empty)
                Newlead.Attributes["mobilephone"] = lead.Téléphone_mobile;
            if (lead.Téléphone_professionnel != string.Empty)
                Newlead.Attributes["telephone1"] = lead.Téléphone_professionnel;
            if (lead.Mail != string.Empty)
                Newlead.Attributes["emailaddress1"] = lead.Mail;
            if (lead.Adresse_1_Rue_1 != string.Empty)
                Newlead.Attributes["address1_line1"] = lead.Adresse_1_Rue_1;
            if (lead.Adresse_1_Rue_2 != string.Empty)
                Newlead.Attributes["address1_line2"] = lead.Adresse_1_Rue_2;
            if (lead.Adresse_1_Rue_3 != string.Empty)
                Newlead.Attributes["address1_line3"] = lead.Adresse_1_Rue_3;
            if (lead.Adresse_1_Rue_4 != string.Empty)
                Newlead.Attributes["vsmp_address1_line4"] = lead.Adresse_1_Rue_4;
            if (lead.Adresse_1_Code_postal != string.Empty)
                Newlead.Attributes["address1_postalcode"] = lead.Adresse_1_Code_postal;
            if (lead.Adresse_1_Ville != string.Empty)
                Newlead.Attributes["address1_city"] = lead.Adresse_1_Ville;
            if (lead.Commentaire != string.Empty)
                Newlead.Attributes["vsmp_commentaire"] = lead.Commentaire;
            if (lead.Adresse2_Rue1 != string.Empty)
                Newlead.Attributes["address2_line1"] = lead.Adresse2_Rue1;
            if (lead.Adresse2_Rue2 != string.Empty)
                Newlead.Attributes["address2_line2"] = lead.Adresse2_Rue2;
            if (lead.Adresse2_Rue3 != string.Empty)
                Newlead.Attributes["address2_line3"] = lead.Adresse2_Rue3;
            if (lead.Adresse_2_Rue_4 != string.Empty)
                Newlead.Attributes["vsmp_address2_line4"] = lead.Adresse_2_Rue_4;
            if (lead.Adresse2_Code_postal != string.Empty)
                Newlead.Attributes["address2_postalcode"] = lead.Adresse2_Code_postal;
            if (lead.Adresse2_Ville != string.Empty)
                Newlead.Attributes["address2_city"] = lead.Adresse2_Ville;
            if (lead.Nom_du_contact_2 != string.Empty)
                Newlead.Attributes["vsmp_nomducontact2"] = lead.Nom_du_contact_2;
            if (lead.Prénom_du_contact_2 != string.Empty)
                Newlead.Attributes["vsmp_prenomducontact2"] = lead.Prénom_du_contact_2;
            if (lead.Téléphone_mobile_2 != string.Empty)
                Newlead.Attributes["vsmp_teelphonemobile2"] = lead.Téléphone_mobile_2;
            if (lead.Téléphone_professionnel_2 != string.Empty)
                Newlead.Attributes["vsmp_telephoneprofessionnel2"] = lead.Téléphone_professionnel_2;
            if (lead.Courrier_électronique_2 != string.Empty)
                Newlead.Attributes["vsmp_courrierelectronique2"] = lead.Courrier_électronique_2;
            if (lead.Nom_du_contact_3 != string.Empty)
                Newlead.Attributes["vsmp_nomducontact3"] = lead.Nom_du_contact_3;
            if (lead.Prénom_du_contact_3 != string.Empty)
                Newlead.Attributes["vsmp_prenomducontact3"] = lead.Prénom_du_contact_3;
            if (lead.Téléphone_mobile_3 != string.Empty)
                Newlead.Attributes["vsmp_telephonemobile3"] = lead.Téléphone_mobile_3;
            if (lead.Téléphone_professionnel_3 != string.Empty)
                Newlead.Attributes["vsmp_telephoneprofessionnel3"] = lead.Téléphone_professionnel_3;
            if (lead.Courrier_électronique_3 != string.Empty)
                Newlead.Attributes["vsmp_courrierlectronique3"] = lead.Courrier_électronique_3;
            if (lead.Adresse_3_Rue_1 != string.Empty)
                Newlead.Attributes["vsmp_adresse3_rue1"] = lead.Adresse_3_Rue_1;
            if (lead.Adresse_3_Rue_2 != string.Empty)
                Newlead.Attributes["vsmp_adresse3_rue2"] = lead.Adresse_3_Rue_2;
            if (lead.Adresse3_Rue3 != string.Empty)
                Newlead.Attributes["vsmp_adresse3_rue3"] = lead.Adresse3_Rue3;
            if (lead.Adresse_3_Rue_4 != string.Empty)
                Newlead.Attributes["vsmp_address3_line4"] = lead.Adresse_3_Rue_4;
            if (lead.Adresse_3_Ville != string.Empty)
                Newlead.Attributes["vsmp_adresse3_ville"] = lead.Adresse_3_Ville;
            if (lead.Adresse_3_Code_postal != string.Empty)
                Newlead.Attributes["vsmp_adresse3_codepostal"] = lead.Adresse_3_Code_postal;
            if (lead.Nom_de_lapporteur != string.Empty)
                Newlead.Attributes["vsmp_nomducontact"] = lead.Nom_de_lapporteur;
            if (lead.Prénom_de_lapporteur != string.Empty)
                Newlead.Attributes["vsmp_prenomducontact"] = lead.Prénom_de_lapporteur;
            if (lead.IDRH != string.Empty)
                Newlead.Attributes["vsmp_idrh"] = lead.IDRH;
            if (lead.Contact != string.Empty)
                Newlead.Attributes["vsmp_contact"] = lead.Contact;
            if (lead.Commentaire_interne_CRC != string.Empty)
                Newlead.Attributes["description"] = lead.Commentaire_interne_CRC;
            
            
        }
        #endregion

        #region Mise-à-jour des apports
        public static void MettreAJourLeads(Lead lead, Entity RetrievedLead, List<Entity> CRMBusinessUnits, out bool Has_Changed)
        {
            Has_Changed = false;
            string[] taboptset = new string[] { " vsmp_statutprospect", "vsmp_motifdabandon", "vsmp_typevsmp", "vsmp_canal", "vsmp_typedeprospect", "vsmp_statutducontrat",
                                                "vsmp_typeapporteur", "vsmp_typecontact", "vsmp_typecontact2", "vsmp_typecontact3", "vsmp_action"} ;


            string[] tabstring = new string[] { "vsmp_refapport", "vsmp_datedouverture", "vsmp_numeroducontrat", "vsmp_veille", "firstname", "lastname", "mobilephone", "telephone1",
                                                 "emailaddress1", "address1_line1", "address1_line2", "address1_line3", "vsmp_address1_line4", "address1_postalcode", "address1_city",
                                                "address2_line1", "address2_line2", "address2_line3", "address2_postalcode", "address2_city",
                                                "vsmp_nomducontact2", "vsmp_prenomducontact2", "vsmp_teelphonemobile2", "vsmp_telephoneprofessionnel2",
                                                "vsmp_courrierelectronique2", "vsmp_nomducontact3", "vsmp_prenomducontact3", "vsmp_telephonemobile3", "vsmp_telephoneprofessionnel3",
                                                "vsmp_courrierlectronique3", "vsmp_adresse3_rue1", "vsmp_adresse3_rue2", "vsmp_adresse3_rue3", "vsmp_adresse3_line4", "vsmp_adresse3_ville",
                                                "vsmp_adresse3_codepostal", "vsmp_nomducontact", "vsmp_prenomducontact", "vsmp_idrh", "vsmp_coderegate",
                                                  "vsmp_contact", "description", "vsmp_commentaire"};

            Entity NewLead = new Entity("lead");
            RenseignerLead(NewLead, lead, CRMBusinessUnits);
            foreach (string elt in tabstring)
            {
                if (NewLead.Contains(elt) && NewLead.GetAttributeValue<string>(elt) != RetrievedLead.GetAttributeValue<string>(elt))
                {
                    RetrievedLead.Attributes[elt] = NewLead.Attributes[elt];
                    Has_Changed = true;
                }
            }
            foreach (string elt in taboptset)
            {
                if ((NewLead.Contains(elt) && !RetrievedLead.Contains(elt)) || (NewLead.Contains(elt) && ((OptionSetValue)NewLead[elt]).Value != ((OptionSetValue)RetrievedLead[elt]).Value))
                {
                    RetrievedLead.Attributes[elt] = NewLead.Attributes[elt];
                    Has_Changed = true;
                }
            }
            if (NewLead.Contains("vsmp_statutdelaction") && NewLead.GetAttributeValue<bool>("vsmp_statutdelaction") != RetrievedLead.GetAttributeValue<bool>("vsmp_statutdelaction"))
            {
                RetrievedLead.Attributes["vsmp_statutdelaction"] = NewLead.Attributes["vsmp_statutdelaction"];
                Has_Changed = true;
            }
            if (NewLead.Contains("vsmp_date") && NewLead.GetAttributeValue<DateTime>("vsmp_date") != RetrievedLead.GetAttributeValue<DateTime>("vsmp_date"))
            {
                RetrievedLead.Attributes["vsmp_date"] = NewLead.Attributes["vsmp_date"];
                Has_Changed = true;
            }
            if (NewLead.Contains("estimatedclosedate") && NewLead.GetAttributeValue<DateTime>("estimatedclosedate") != RetrievedLead.GetAttributeValue<DateTime>("estimatedclosedate"))
            {
                RetrievedLead.Attributes["estimatedclosedate"] = NewLead.Attributes["estimatedclosedate"];
                Has_Changed = true;
            }
        }
        #endregion

        #region Recherche d'une division par code regate
        public static Guid GetBU(string CodeRegate, List<Entity> CRMBusinessUnits)
        {
            if (CRMBusinessUnits != null && CRMBusinessUnits.Any(entity => entity.GetAttributeValue<string>("vsmp_code") == CodeRegate))
                return CRMBusinessUnits.Find(entity => entity.GetAttributeValue<string>("vsmp_code") == CodeRegate).Id;
            else
                return Guid.Empty;
        }
        #endregion

        #region Création multiple des records
        public static void CreateMultiple(IOrganizationService _orgService, EntityCollection input)
        {
            
            int i = 1;
            int nb_records = 1;
            int nb_responses = 0;
            ExecuteMultipleResponse responseWithResults = null;
            ExecuteMultipleRequest requestWithResults = new ExecuteMultipleRequest()
            {
                // Assigner les paramètre définissant le comportement de l'exécution : continuer en cas d'erreur, retourner des réponses
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            LogHelper.Writer(string.Format("Début de l'upsert CRM des records de l'entité {0}, {1} records à traiter en total", input.EntityName, input.Entities.Count));
            foreach (var entity in input.Entities)
            {
                UpsertRequest upsertrequest = new UpsertRequest { Target = entity };
                requestWithResults.Requests.Add(upsertrequest);
                if (i % 500 == 0 || i == input.Entities.Count)          //upsert des records par batch de 500 records
                {
                    try
                    {
                        //Exécuter toutes les requetes upsert de la collection par un seul appel web.                       
                        responseWithResults = (ExecuteMultipleResponse)_orgService.Execute(requestWithResults);
                        if (responseWithResults.Responses.Count > 0)
                        {

                            foreach (var responseItem in responseWithResults.Responses)
                            {
                                if (responseItem.Fault != null)
                                    DisplayFault(requestWithResults.Requests[responseItem.RequestIndex], responseItem.RequestIndex, responseItem.Fault);
                            }
                        }
                        requestWithResults.Requests = null;
                        requestWithResults.Requests = new OrganizationRequestCollection();
                        LogHelper.Writer(string.Format("# {0} recrods traités", nb_records));
                        nb_records = 0;
                        nb_responses += responseWithResults.Responses.Count;

                    }
                    catch (Exception ex)
                    {
                        LogHelper.Writer("UPSERT ERROR, type d'erreur : " + ex.ToString());
                    }
                }
                i++;
                nb_records++;
            }
            LogHelper.Writer(string.Format("# nombre de réponses à l'upsert : {0}", nb_responses));
            LogHelper.Writer(string.Format("Fin de l'upsert, {0} records traités" + Environment.NewLine, i - 1));
        }

        #endregion

        #region affichage des erreurs upsert
        static private void DisplayFault(OrganizationRequest organizationRequest, int count, OrganizationServiceFault organizationServiceFault)
        {
            LogHelper.Writer(string.Format("une erreur est survenue dans le traitement de la requete {1}, à l'index {0} dans la collection d'entités avec le message suivant : {2}", count + 1,
                organizationRequest.RequestName, organizationServiceFault.Message));
        }
        #endregion
    }
}
