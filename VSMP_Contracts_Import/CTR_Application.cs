﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk.Messages;
using Log;
using System.Globalization;

namespace VSMP_Contracts
{
    public static class CTR_Application
    {
        #region Récupération de tous les records d'une entité donnée depuis le CRM
        public static List<Entity> GetRecords(IOrganizationService _orgService, string EntityLogicalName, string[] CRMFields)
        {
            List<EntityCollection> recordsRetrieved = new List<EntityCollection>();
            int querycount = 5000;
            int pagenumber = 1;
            List<Entity> CRMRecords = new List<Entity>();
            QueryExpression query = new QueryExpression(EntityLogicalName)
            { ColumnSet = new ColumnSet(CRMFields) };
            query.PageInfo = new PagingInfo();
            query.PageInfo.Count = querycount;
            query.PageInfo.PageNumber = pagenumber;
            query.PageInfo.PagingCookie = null;

            while (true)
            {
                EntityCollection newCollection = _orgService.RetrieveMultiple(query);
                recordsRetrieved.Add(newCollection);
                if (newCollection.MoreRecords)
                {
                    query.PageInfo.PageNumber++;
                    query.PageInfo.PagingCookie = newCollection.PagingCookie;
                }
                else
                    break;
            }
            if (recordsRetrieved == null || recordsRetrieved.Count < 1 || recordsRetrieved[0].Entities == null)
                return null;
            else
            {
                foreach (EntityCollection collection in recordsRetrieved)
                {
                    foreach (Entity entity in collection.Entities)
                        CRMRecords.Add(entity);
                }
                return CRMRecords;
            }
        }
        #endregion

        #region Recherche d'un contact dans CRM
        public static Entity GetContact(List<Entity> CRMContacts, string customerId = null, string telephone = null, string mobilephone = null)
        {
            if (CRMContacts != null)
                return CRMContacts.Find(entity => entity.GetAttributeValue<string>("vsmp_identifiantclient") == customerId &&
                                                  entity.GetAttributeValue<string>("telephone1") == telephone &&
                                                  entity.GetAttributeValue<string>("mobilephone") == mobilephone);
            else
                return null;
        }
        #endregion Recherche d'un contact dans CRM

        #region Renseigner les champs d'un contact
        public static void RenseignerContact<T>(Entity NewContact, Result contrat, Trustedcontact trustcont = null)
        {
            if (typeof(T) == typeof(Subscriber))
            {
                switch (contrat.subscriber.civility)
                {
                    case "M":
                        NewContact.Attributes["gendercode"] = new OptionSetValue(1);
                        break;
                    case "MME":
                        NewContact.Attributes["gendercode"] = new OptionSetValue(2);
                        break;
                    default:
                        break;
                }
                NewContact.Attributes["firstname"] = contrat.subscriber.firstName;
                NewContact.Attributes["lastname"] = contrat.subscriber.lastName;
                NewContact.Attributes["vsmp_identifiantclient"] = contrat.subscriber.customerId;
                NewContact.Attributes["vsmp_clientnonnumerique"] = contrat.subscriber.offlineClient;
                if (contrat.subscriber.birthDate != null)
                    NewContact.Attributes["vsmp_datedenaissance"] = contrat.subscriber.birthDate;
                if (contrat.subscriber.phoneNumbers != null && contrat.subscriber.phoneNumbers.Length != 0)
                {
                    foreach (Phonenumber phone in contrat.subscriber.phoneNumbers)
                    {
                        if (phone.type == "LANDLINE")
                            NewContact.Attributes["telephone1"] = phone.number;
                        else if (phone.type == "MOBILE")
                            NewContact.Attributes["mobilephone"] = phone.number;
                    }
                }
                NewContact.Attributes["emailaddress1"] = contrat.subscriber.mail;
                NewContact.Attributes["address1_line1"] = contrat.subscriber.adressL2;
                NewContact.Attributes["address1_line2"] = contrat.subscriber.adressL3;
                NewContact.Attributes["address1_line3"] = contrat.subscriber.adressL4;
                NewContact.Attributes["vsmp_address1_line4"] = contrat.subscriber.adressL5;

                string[] codepostal_Ville = contrat.subscriber.adressL6.Split(' ');

                string city=string.Empty;
                NewContact.Attributes["address1_postalcode"] = codepostal_Ville[0];
                if (codepostal_Ville.Length > 1)
                {
                    for (int i = 1; i < codepostal_Ville.Length; i++)
                    {
                        city = city + codepostal_Ville[i];
                        if (i < codepostal_Ville.Length - 1)
                            city = city + " ";
                    }
                    NewContact.Attributes["address1_city"] = city;
                }
                NewContact.Attributes["address1_country"] = contrat.subscriber.countryCode;
            }
            else if (typeof(T) == typeof(Recipient))
            {
                switch (contrat.recipient.civility)
                {
                    case "M":
                        NewContact.Attributes["gendercode"] = new OptionSetValue(1);
                        break;
                    case "MME":
                        NewContact.Attributes["gendercode"] = new OptionSetValue(2);
                        break;
                    default:
                        break;
                }
                NewContact.Attributes["vsmp_identifiantclient"] = contrat.recipient.customerId;
                bool civ = contrat.recipient.doctorCivility == "M";
                NewContact.Attributes["vsmp_civilitemedecin"] = civ;
                NewContact.Attributes["vsmp_nomdumedecin"] = contrat.recipient.doctorName;
                NewContact.Attributes["vsmp_telephonemedecin"] = contrat.recipient.doctorPhone;
                switch (contrat.recipient.phoneInstallationType)
                {
                    case "BOX":
                        NewContact.Attributes["vsmp_typedinstallationtelephonique"] = new OptionSetValue(688760000);
                        break;
                    case "LANDLINE":
                        NewContact.Attributes["vsmp_typedinstallationtelephonique"] = new OptionSetValue(688760001);
                        break;
                    case "OTHER":
                        NewContact.Attributes["vsmp_typedinstallationtelephonique"] = new OptionSetValue(688760002);
                        break;
                    default:
                        break;
                }
                NewContact.Attributes["firstname"] = contrat.recipient.firstName;
                NewContact.Attributes["lastname"] = contrat.recipient.lastName;
                if (contrat.recipient.birthDate != null)
                    NewContact.Attributes["vsmp_datedenaissance"] = contrat.recipient.birthDate;
                if (contrat.recipient.phoneNumbers != null && contrat.recipient.phoneNumbers.Length != 0)
                {
                    foreach (Phonenumber1 phone in contrat.recipient.phoneNumbers)
                    {
                        if (phone.type == "LANDLINE")
                            NewContact.Attributes["telephone1"] = phone.number;
                        else if (phone.type == "MOBILE")
                            NewContact.Attributes["mobilephone"] = phone.number;
                    }
                }
                NewContact.Attributes["emailaddress1"] = contrat.recipient.mail;
                NewContact.Attributes["address1_line1"] = contrat.recipient.adressL2;
                NewContact.Attributes["address1_line2"] = contrat.recipient.adressL3;
                NewContact.Attributes["address1_line3"] = contrat.recipient.adressL4;
                NewContact.Attributes["vsmp_address1_line4"] = contrat.recipient.adressL5;
                string[] codepostal_Ville = contrat.recipient.adressL6.Split(' ');
                string city = string.Empty;
                NewContact.Attributes["address1_postalcode"] = codepostal_Ville[0];
                if (codepostal_Ville.Length > 1)
                {
                    for (int i = 1; i < codepostal_Ville.Length; i++)
                    {
                        city = city + codepostal_Ville[i];
                        if (i < codepostal_Ville.Length - 1)
                            city = city + " ";
                    }
                    NewContact.Attributes["address1_city"] = city;
                }
            }
            else
            {
                switch (trustcont.civility)
                {
                    case "M":
                        NewContact.Attributes["gendercode"] = new OptionSetValue(1);
                        break;
                    case "MME":
                        NewContact.Attributes["gendercode"] = new OptionSetValue(2);
                        break;
                    default:
                        break;
                }
                NewContact.Attributes["vsmp_identifiantclient"] = trustcont.customerId;
                NewContact.Attributes["firstname"] = trustcont.firstName;
                NewContact.Attributes["lastname"] = trustcont.lastName;
                NewContact.Attributes["vsmp_possedeundoubledescles"] = trustcont.hasKeys;
                if (trustcont.travelTimeInMinutes != 0)
                    NewContact.Attributes["vsmp_dureedutrajet"] = (trustcont.travelTimeInMinutes).ToString();
                if (trustcont.birthDate != null)
                    NewContact.Attributes["vsmp_datedenaissance"] = trustcont.birthDate;
                if (trustcont.phoneNumbers != null && trustcont.phoneNumbers.Length != 0)
                {
                    foreach (Phonenumber2 phone in trustcont.phoneNumbers)
                    {
                        if (phone.type == "LANDLINE")
                            NewContact.Attributes["telephone1"] = phone.number;
                        else if (phone.type == "MOBILE")
                            NewContact.Attributes["mobilephone"] = phone.number;
                    }
                }
                NewContact.Attributes["emailaddress1"] = trustcont.mail;
                NewContact.Attributes["address1_line1"] = trustcont.adressL2;
                NewContact.Attributes["address1_line2"] = trustcont.adressL3;
                NewContact.Attributes["address1_line3"] = trustcont.adressL4;
                NewContact.Attributes["vsmp_address1_line4"] = trustcont.adressL5;
                string[] codepostal_Ville = trustcont.adressL6.Split(' ');
                string city = string.Empty;
                NewContact.Attributes["address1_postalcode"] = codepostal_Ville[0];
                if (codepostal_Ville.Length > 1)
                {
                    for (int i = 1; i < codepostal_Ville.Length; i++)
                    {
                        city = city + codepostal_Ville[i];
                        if (i < codepostal_Ville.Length - 1)
                            city = city + " ";
                    }
                    NewContact.Attributes["address1_city"] = city;
                }
            }
        }
        #endregion 

        #region Renseigner les champs d'un contrat 
        public static void RenseignerContrat(Entity Contract, Result contrat, List<Entity> CRMBusinessUnits)
        {
            Contract.Attributes["vsmp_name"] = contrat.contractNumber; //string

            //Date de création
            string contratnum = Contract.Attributes["vsmp_name"].ToString();
            string[] stringsplits = contratnum.Split(new string[] { "-" }, StringSplitOptions.None);
            string firstPart = stringsplits[0];
            char[] MyChar = { 'V', 'S', 'M', 'P', ' ' };
            string NewString = firstPart.TrimStart(MyChar);
            //var ci = new CultureInfo("en-EN");
            DateTime MyDate = DateTime.ParseExact(NewString, "ddMMyy", null);
            Contract.Attributes["vsmp_datedecreation"] = MyDate;

        


          
            if (contrat.metadata.recipient != null)
            {
                Contract.Attributes["vsmp_medaillon"] = contrat.metadata.recipient.secondLocket != null ? contrat.metadata.recipient.secondLocket : false; //bool
            }

            //Code Lead - Ref Apport
            if (contrat.metadata.crc != null)
            {
                Contract.Attributes["vsmp_referenceapport"] = contrat.metadata.crc.leadCode;
            }

            if (contrat.version != 0)
                Contract.Attributes["vsmp_version"] = contrat.version; //int
            //20171018 HZU - Ajout mapping (CRM)vsmp_teleassistance = (HUB)remoteAssistance
            Contract.Attributes["vsmp_teleassistance"] = contrat.remoteAssistance; //bool
            //20171018 HZU - Ajout mapping (CRM)vsmp_testcontract = (HUB)metadata.test
            Contract.Attributes["vsmp_testcontract"] = contrat.metadata.test != null ? contrat.metadata.test : false; //bool
            //20171012 HZU - Correction mapping (CRM)vsmp_adresseducontrat = (HUB).recipient.adressL2
            Contract.Attributes["vsmp_adresseducontrat"] = contrat.recipient.adressL2 + " " + contrat.recipient.adressL3 + " " + contrat.recipient.adressL4 + " " + contrat.recipient.adressL5 + " " + contrat.recipient.adressL6;
            Contract.Attributes["vsmp_typedetarif"] = new OptionSetValue(688760000); //optinosetvalue
            if (contrat.metadata != null && contrat.metadata.retailInformations != null && GetBU(contrat.metadata.retailInformations.regateCode, CRMBusinessUnits) != Guid.Empty)
                Contract.Attributes["vsmp_coderegate"] = contrat.metadata.retailInformations.regateCode;
            if (contrat.legalInfo != null)
            {
                if (contrat.legalInfo.beginDate != null) //legalinfo
                {
                    Contract.Attributes["vsmp_datedactivation"] = contrat.legalInfo.beginDate; //date 
                    Contract.Attributes["vsmp_datedefin"] = contrat.legalInfo.beginDate.Value.AddMonths(contrat.legalInfo.contractDurationInMonths); //date
                }
                switch (contrat.legalInfo.contractStatus) //legal info
                {

                    case "CREATED":
                        Contract.Attributes["vsmp_statut"] = new OptionSetValue(688760000);
                        break;
                    case "COMPLETE":
                        Contract.Attributes["vsmp_statut"] = new OptionSetValue(688760001);
                        //Date de souscription
                        if (!Contract.Contains("vsmp_datedesouscription") || String.IsNullOrEmpty(Contract.Attributes["vsmp_datedesouscription"].ToString()))
                        {
                            if (contrat.dateModification != null)
                            {
                                Contract.Attributes["vsmp_datedesouscription"] = contrat.dateModification.Value;
                            }
                        }
                        break;
                    case "VALIDATED":
                        Contract.Attributes["vsmp_statut"] = new OptionSetValue(688760002);
                        break;
                    case "SUSPENDED":
                        Contract.Attributes["vsmp_statut"] = new OptionSetValue(688760003);
                        break;
                    case "PENDING_CANCELLATION":
                        Contract.Attributes["vsmp_statut"] = new OptionSetValue(688760004);
                        break;
                    case "CANCELLED":
                        Contract.Attributes["vsmp_statut"] = new OptionSetValue(688760005);
                        if (!Contract.Contains("vsmp_datedannulation") || String.IsNullOrEmpty(Contract.Attributes["vsmp_datedannulation"].ToString()))
                        {
                            if (contrat.dateModification != null)
                            {
                                Contract.Attributes["vsmp_datedannulation"] = contrat.dateModification.Value;
                            }
                        }
                        break;
                    default:
                        break;
                }
                if (contrat.legalInfo.terminationRequestDate != null)  //legalinfo
                    Contract.Attributes["vsmp_datederesiliation"] = contrat.legalInfo.terminationRequestDate; //date
                switch (contrat.legalInfo.terminationReason) //legalinfo
                {

                    case "BENEFICIARY_DEATH":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760000);
                        break;
                    case "CANCELLATION_DESK":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760001);
                        break;
                    case "INAPPROPRIATE_OR_NOT_USED_SERVICE":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760004);
                        break;
                    case "LONG_TERM_HOSPITALIZATION":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760005);
                        break;
                    case "MOVING_SPECIALIZED_INSTITUTION":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760007);
                        break;
                    case "POSTMAN_SERVICE_NOT_MEET_EXPECTATION":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760006);
                        break;
                    case "RELOCATION":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760002);
                        break;
                    case "REMOTE_ASSISTANCE_NOT_MEET_EXPECTATION":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760008);
                        break;
                    case "RETRACTATION":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760009);
                        break;
                    case "TERMINATED_BY_PROVIDER":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760011);
                        break;
                    case "MALICIOUS_ACTS":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760003);
                        break;
                    case "TOO_HIGH_PRICE":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760010);
                        break;
                    case "BENEFICIARY_DECLINED":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760012);
                        break;
                    case "FINANCIAL_DIFFICULTIES":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760013);
                        break;
                    case "OTHERS":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760014);
                        break;
                    case "POSTMAN_COMPLAINT":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760015);
                        break;
                    case "SUBSCRIBER_REQUEST":
                        Contract.Attributes["vsmp_raisondannulation"] = new OptionSetValue(688760014);
                        break;
                    default:
                        break;
                }
            }


            if (contrat.payment != null)
            {
                switch (contrat.payment.type) //payment
                {
                    case "CB":
                        Contract.Attributes["vsmp_typedepaiement"] = new OptionSetValue(688760000);
                        break;
                    case "SEPA":
                        Contract.Attributes["vsmp_typedepaiement"] = new OptionSetValue(688760001);
                        break;
                    default:
                        break;
                }
                Contract.Attributes["vsmp_bic"] = contrat.payment.BIC; //string   
                Contract.Attributes["vsmp_iban"] = contrat.payment.IBAN; //string 
                Contract.Attributes["vsmp_prixttc"] = new Money((contrat.payment.ttcPriceInCents) / 100); //money  
                if (contrat.payment.htPriceInCents != 0)
                    Contract.Attributes["vsmp_prixnet"] = new Money((contrat.payment.htPriceInCents) / 100); //money
                if (contrat.payment.tvaAmountInCents != 0)
                    Contract.Attributes["vsmp_montanttva"] = new Money((contrat.payment.tvaAmountInCents) / 100); //money
                Contract.Attributes["vsmp_payeur"] = contrat.payment.accountOwner;
            }
            if (contrat.delivery != null)
            {
                if (contrat.delivery.visitDays != null && contrat.delivery.visitDays.Length != 0)
                {
                    for (int i = 0; i < contrat.delivery.visitDays.Length; i++)
                    {
                        if (contrat.delivery.visitDays[i] == "MONDAY")
                            Contract.Attributes["vsmp_lundi"] = true;     //bool
                        else if (contrat.delivery.visitDays[i] == "TUESDAY")
                            Contract.Attributes["vsmp_mardi"] = true;
                        else if (contrat.delivery.visitDays[i] == "WEDNESDAY")
                            Contract.Attributes["vsmp_mercredi"] = true;
                        else if (contrat.delivery.visitDays[i] == "THURSDAY")
                            Contract.Attributes["vsmp_jeudi"] = true;
                        else if (contrat.delivery.visitDays[i] == "FRIDAY")
                            Contract.Attributes["vsmp_vendredi"] = true;
                        else if (contrat.delivery.visitDays[i] == "SATURDAY")
                            Contract.Attributes["vsmp_samedi"] = true;
                    }
                }
                if (contrat.delivery.suspensionDate != null)
                    Contract.Attributes["vsmp_datedesuspension"] = contrat.delivery.suspensionDate;
                switch (contrat.delivery.suspensionReason)
                {
                    case "HOLIDAYS":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760000);
                        break;
                    case "HOSPITALIZATION":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760001);
                        break;
                    case "OTHERS":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760002);
                        break;
                    case "SUSPENDED_BY_PROVIDER":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760003);
                        break;
                    case "INQUIRY_FOR_MALICIOUS_ACTS":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760004);
                        break;
                    case "POSTMAN_BENEFICIARY_STOP_REQUEST":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760005);
                        break;
                    case "UNKNOWN_ADDRESS":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760006);
                        break;
                    case "UNKNOWN_BENEFICIARY":
                        Contract.Attributes["vsmp_raisondelasuspension"] = new OptionSetValue(688760007);
                        break;
                    default:
                        break;
                }
                if (contrat.delivery.startDate != null)
                    Contract.Attributes["vsmp_demarragedelaprestation"] = contrat.delivery.startDate; //date
                Contract.Attributes["vsmp_veille"] = contrat.delivery.formula; //string
            }

        }
        #endregion 

        #region Renseigner une connexion contrat/contact
        public static void Renseigner_ContratContact_Connection<T>(Entity connection, Result contrat, Guid contactid, Guid contractid, List<Entity> CRMConnectionRoles, Trustedcontact trustedcon = null)
        {
            connection.Attributes["record1id"] = new EntityReference("vsmp_contrat", contractid);   //associer le contrat
            connection.Attributes["record2id"] = new EntityReference("contact", contactid);         //associer le contact

            if (typeof(T) == typeof(Subscriber))
            {
                connection.Attributes["record2roleid"] = new EntityReference("connectionrole", RetrieveConnectionRole("Souscripteur", CRMConnectionRoles));
                switch (contrat.recipient.subscribersRelationship)
                {
                    case "FRIEND":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760001);
                        break;
                    case "OTHER":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760000);
                        break;
                    case "CHILD":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760005);
                        break;
                    case "GRANDCHILD":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760002);
                        break;
                    case "ASSOCIATION":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760008);
                        break;
                    case "FAMILY_MEMBER":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760006);
                        break;
                }
            }
            else if (typeof(T) == typeof(Trustedcontact))
            {

                connection.Attributes["record2roleid"] = new EntityReference("connectionrole", RetrieveConnectionRole("Contact de proximité", CRMConnectionRoles));
                switch (trustedcon.contactType)
                {
                    case "NEIGHBOUR":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760003);
                        break;
                    case "FAMILY":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760006);
                        break;
                    case "FRIEND":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760001);
                        break;
                    case "ASSOCIATION_MEMBER":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760007);
                        break;
                    case "OTHER":
                        connection.Attributes["vsmp_lien"] = new OptionSetValue(688760000);
                        break;
                }

            }
            else
                connection.Attributes["record2roleid"] = new EntityReference("connectionrole", RetrieveConnectionRole("Bénéficiaire", CRMConnectionRoles));
        }
        #endregion 

        #region Renseigner une connexion contact/contact
        public static void Renseigner_Contacts_Connection<T>(Entity connection, Result contrat, Guid recId, Guid contact2Id, List<Entity> CRMConnectionRoles, Trustedcontact trustedcon = null)
        {
            connection.Attributes["record1id"] = new EntityReference("contact", recId);
            connection.Attributes["record2id"] = new EntityReference("contact", contact2Id);
            Guid id = Guid.Empty;
            Guid id2 = Guid.Empty;
            if (typeof(T) == typeof(Subscriber))
            {
                switch (contrat.recipient.subscribersRelationship)
                {
                    case "FRIEND":
                        id = RetrieveConnectionRole("Ami", CRMConnectionRoles);
                        id2 = id;
                        break;
                    case "OTHER":
                        id = RetrieveConnectionRole("Autre", CRMConnectionRoles);
                        id2 = id;
                        break;
                    case "CHILD":
                        id = RetrieveConnectionRole("Enfant", CRMConnectionRoles);
                        id2 = RetrieveConnectionRole("Parent", CRMConnectionRoles);
                        break;
                    case "GRANDCHILD":
                        id = RetrieveConnectionRole("Petit enfant", CRMConnectionRoles);
                        id2 = RetrieveConnectionRole("Grand parent", CRMConnectionRoles);
                        break;
                    case "ASSOCIATION":
                        id = RetrieveConnectionRole("Association", CRMConnectionRoles);
                        break;
                    case "FAMILY_MEMBER":
                        id = RetrieveConnectionRole("Famille", CRMConnectionRoles);
                        id2 = id;
                        break;
                }
            }
            else
            {
                switch (trustedcon.contactType)
                {
                    case "FRIEND":
                        id = RetrieveConnectionRole("Ami", CRMConnectionRoles);
                        id2 = id;
                        break;
                    case "OTHER":
                        id = RetrieveConnectionRole("Autre", CRMConnectionRoles);
                        id2 = id;
                        break;
                    case "NEIGHBOUR":
                        id = RetrieveConnectionRole("Voisin", CRMConnectionRoles);
                        id2 = id;
                        break;
                    case "ASSOCIATION_MEMBER":
                        id = RetrieveConnectionRole("Association", CRMConnectionRoles);
                        break;
                    case "FAMILY":
                        id = RetrieveConnectionRole("Famille", CRMConnectionRoles);
                        id2 = id;
                        break;
                }
            }
            if (id != Guid.Empty)
                connection.Attributes["record2roleid"] = new EntityReference("connectionrole", id);
            if (id2 != Guid.Empty)
                connection.Attributes["record1roleid"] = new EntityReference("connectionrole", id2);

        }
        #endregion

        #region Recherche d'une connexion contact/contact
        public static Entity Retrieve_Contacts_Connection(Guid benId, Guid contId2, List<Entity> CRMConnections)
        {
            if (CRMConnections != null)
                return CRMConnections.Find(entity => ((EntityReference)entity["record1id"]).Id == benId && ((EntityReference)entity["record2id"]).Id == contId2);
            else
                return null;
        }

        #endregion

        #region Recherche d'une connexion contrat/contact
        public static Entity Retrieve_ContratContact_Connection(Guid contractId, Guid roleid, List<Entity> CRMConnections, Guid? trustedconId = null)
        {
            if (CRMConnections != null)
            {
                if (trustedconId != null)
                    return CRMConnections.Find(entity => ((EntityReference)entity["record1id"]).Id == contractId && ((EntityReference)entity["record2id"]).Id == trustedconId && ((EntityReference)entity["record2roleid"]).Id == roleid);

                else
                    return CRMConnections.Find(entity => ((EntityReference)entity["record1id"]).Id == contractId && ((EntityReference)entity["record2roleid"]).Id == roleid);
            }
            else
                return null;
        }
        #endregion

        #region Recherhe d'un role de connexion dans CRM
        public static Guid RetrieveConnectionRole(string connectionRoleName, List<Entity> CRMConnectionRoles)
        {
            Entity ConnectionRole = null;
            if (CRMConnectionRoles != null && CRMConnectionRoles.Any(entity => entity.GetAttributeValue<string>("name") == connectionRoleName))
            {
                ConnectionRole = CRMConnectionRoles.Find(entity => entity.GetAttributeValue<string>("name") == connectionRoleName);
                return ConnectionRole.Id;
            }
            else
                return Guid.Empty;
        }
        #endregion

        #region mise à jour des contrat
        public static void MiseAjourContrat(Result contrat, Entity RetrievedContract, List<Entity> CRMBusinessUnits)
        {

            string[] tabstring = new string[] { "vsmp_name", "vsmp_bic", "vsmp_iban", "vsmp_veille", "vsmp_payeur", "vsmp_coderegate", "vsmp_referenceapport" };
            string[] tabdate = new string[] { "vsmp_datedactivation", "vsmp_datedecreation", "vsmp_datedefin", "vsmp_datedannulation", "vsmp_datedesuspension", "vsmp_demarragedelaprestation", "vsmp_datedesouscription", "vsmp_datederesiliation" };
            string[] taboptset = new string[] { "vsmp_typedepaiement", "vsmp_statut", "vsmp_raisondannulation", "vsmp_typedetarif", "vsmp_raisondelasuspension" };
            string[] tabint = new string[] { "vsmp_version" };
            string[] tabmoney = new string[] { "vsmp_prixttc", "vsmp_prixnet", "vsmp_montanttva" };
            string[] tabbool = new string[] { "vsmp_lundi", "vsmp_mardi", "vsmp_mercredi", "vsmp_jeudi", "vsmp_vendredi", "vsmp_samedi", "vsmp_testcontract", "vsmp_medaillon", "vsmp_teleassistance" };
            Entity contratcourant = new Entity("vsmp_contrat");
            RenseignerContrat(contratcourant, contrat, CRMBusinessUnits);
            foreach (string elt in tabstring)
            {
                if (contratcourant.Contains(elt) && contratcourant.GetAttributeValue<string>(elt) != RetrievedContract.GetAttributeValue<string>(elt))
                    RetrievedContract.Attributes[elt] = contratcourant.Attributes[elt];
            }
            foreach (string elt in tabdate)
            {
                if (contratcourant.Contains(elt) && contratcourant.GetAttributeValue<DateTime>(elt) != RetrievedContract.GetAttributeValue<DateTime>(elt))
                    RetrievedContract.Attributes[elt] = contratcourant.Attributes[elt];
            }
            foreach (string elt in taboptset)
            {
                if ((contratcourant.Contains(elt) && !RetrievedContract.Contains(elt)) || (contratcourant.Contains(elt) &&
                    ((OptionSetValue)contratcourant[elt]).Value != ((OptionSetValue)RetrievedContract[elt]).Value))
                    RetrievedContract.Attributes[elt] = contratcourant.Attributes[elt];
            }
            foreach (string elt in tabbool)
            {
                if (contratcourant.Contains(elt))
                {
                    if (contratcourant.GetAttributeValue<bool>(elt) != RetrievedContract.GetAttributeValue<bool>(elt))
                        RetrievedContract.Attributes[elt] = contratcourant.Attributes[elt];
                }
                else
                    RetrievedContract.Attributes[elt] = false;
            }
            foreach (string elt in tabmoney)
            {

                if ((contratcourant.Contains(elt) && !RetrievedContract.Contains(elt)) || (contratcourant.Contains(elt) &&
                    ((Money)contratcourant[elt]).Value != ((Money)RetrievedContract[elt]).Value))
                    RetrievedContract.Attributes[elt] = contratcourant.Attributes[elt];
            }
            if (contratcourant.Contains("vsmp_version") && contratcourant.GetAttributeValue<int>("vsmp_version") != RetrievedContract.GetAttributeValue<int>("vsmp_version"))
                RetrievedContract.Attributes["vsmp_version"] = contratcourant.Attributes["vsmp_version"];

        }
        #endregion

        #region Mise à jour d'un contact
        public static void MettreAjour<T>(Result contrat, Entity retrievedcontact, Trustedcontact trustcont = null)
        {
            string[] tabstring = new string[] {"emailaddress1", "mobilephone", "telephone1", "vsmp_dureedutrajet", "firstname", "lastname",
                                                "vsmp_nomdumedecin", "vsmp_telephonemedecin", "vsmp_identifiantclient", "address1_line1", "address1_line2", "address1_line3", "vsmp_address1_line4",
                                                 "address1_city", "address1_postalcode", "address1_country"};
            string[] tabbool = new string[] { "vsmp_possedeundoubledescles", "vsmp_civilitemedecin" };
            string[] taboptset = new string[] { "gendercode", "vsmp_typedinstallationtelephonique" };
            string[] tabdate = new string[] { "vsmp_datedenaissance" };

            Entity contactcourant = new Entity("contact");
            RenseignerContact<T>(contactcourant, contrat, trustcont);
            foreach (string elt in tabstring)
            {
                if (contactcourant.Contains(elt) && contactcourant.GetAttributeValue<string>(elt) != retrievedcontact.GetAttributeValue<string>(elt))
                    retrievedcontact.Attributes[elt] = contactcourant.Attributes[elt];
            }
            foreach (string elt in tabdate)
            {
                if (contactcourant.Contains(elt) && contactcourant.GetAttributeValue<DateTime>(elt) != retrievedcontact.GetAttributeValue<DateTime>(elt))
                    retrievedcontact.Attributes[elt] = contactcourant.Attributes[elt];
            }
            foreach (string elt in taboptset)
            {
                if ((contactcourant.Contains(elt) && !retrievedcontact.Contains(elt)) || (contactcourant.Contains(elt) &&
                    ((OptionSetValue)contactcourant[elt]).Value != ((OptionSetValue)retrievedcontact[elt]).Value))
                    retrievedcontact.Attributes[elt] = contactcourant.Attributes[elt];
            }
            foreach (string elt in tabbool)
            {
                if (contactcourant.Contains(elt) && contactcourant.GetAttributeValue<bool>(elt) != retrievedcontact.GetAttributeValue<bool>(elt))
                    retrievedcontact.Attributes[elt] = contactcourant.Attributes[elt];
            }

        }
        #endregion

        #region Recherche d'une division par code regate
        public static Guid GetBU(string CodeRegate, List<Entity> CRMBusinessUnits)
        {
            if (CRMBusinessUnits != null && CRMBusinessUnits.Any(entity => entity.GetAttributeValue<string>("vsmp_code") == CodeRegate))
                return CRMBusinessUnits.Find(entity => entity.GetAttributeValue<string>("vsmp_code") == CodeRegate).Id;
            else
                return Guid.Empty;
        }
        #endregion

        #region Création multiple des records
        public static void CreateMultiple(IOrganizationService _orgService, EntityCollection input)
        {
            int i = 1;
            int nb_records = 1;
            int nb_responses = 0;
            ExecuteMultipleResponse responseWithResults = null;
            ExecuteMultipleRequest requestWithResults = new ExecuteMultipleRequest()
            {
                // Assigner les paramètre définissant le comportement de l'exécution : continuer en cas d'erreur, retourner des réponses
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            LogHelper.Writer(string.Format("Début de l'upsert CRM des records de l'entité {0}, {1} records à traiter en total", input.EntityName, input.Entities.Count));
            foreach (var entity in input.Entities)
            {
                UpsertRequest upsertrequest = new UpsertRequest { Target = entity };
                requestWithResults.Requests.Add(upsertrequest);
                if (i % 500 == 0 || i == input.Entities.Count)          //upsert des records par batch de 500 records
                {
                    try
                    {
                        //Exécuter toutes les requetes upsert de la collection par un seul appel web.                       
                        responseWithResults = (ExecuteMultipleResponse)_orgService.Execute(requestWithResults);
                        if (responseWithResults.Responses.Count > 0)
                        {

                            foreach (var responseItem in responseWithResults.Responses)
                            {
                                if (responseItem.Fault != null)
                                    DisplayFault(requestWithResults.Requests[responseItem.RequestIndex], responseItem.RequestIndex, responseItem.Fault);
                            }
                        }
                        requestWithResults.Requests = null;
                        requestWithResults.Requests = new OrganizationRequestCollection();
                        LogHelper.Writer(string.Format("# {0} recrods traités", nb_records));
                        nb_records = 0;
                        nb_responses += responseWithResults.Responses.Count;

                    }
                    catch (Exception ex)
                    {
                        LogHelper.Writer("UPSERT ERROR, type d'erreur : " + ex.ToString());
                    }
                }
                i++;
                nb_records++;
            }
            LogHelper.Writer(string.Format("# nombre de réponses à l'upsert : {0}", nb_responses));
            LogHelper.Writer(string.Format("Fin de l'upsert, {0} records traités" + Environment.NewLine, i - 1));
        }

        #endregion

        #region affichage des erreurs upsert
        static private void DisplayFault(OrganizationRequest organizationRequest, int count, OrganizationServiceFault organizationServiceFault)
        {
            LogHelper.Writer(string.Format("### une erreur est survenue dans le traitement de la requete {1},ç l'index {0} dans la collection d'entités avec le message suivant : {2}", count + 1,
                organizationRequest.RequestName, organizationServiceFault.Message));
        }
        #endregion

        #region Assignation des paramètres de requete getcontact
        public static void AssignParameters<T>(Result contrat, out string customerId, out string telephonefixe, out string mobilephone, Trustedcontact trustedcon = null)
        {
            customerId = null;
            telephonefixe = null;
            mobilephone = null;
            if (typeof(T) == typeof(Subscriber))
            {
                if (contrat.subscriber.customerId != null)
                    customerId = contrat.subscriber.customerId;
                if (contrat.subscriber.phoneNumbers != null && contrat.subscriber.phoneNumbers.Length != 0)
                {
                    foreach (Phonenumber phone in contrat.subscriber.phoneNumbers)
                    {
                        if (phone.type == "LANDLINE")
                            telephonefixe = phone.number;
                        else if (phone.type == "MOBILE")
                            mobilephone = phone.number;
                    }
                }
            }
            else if (typeof(T) == typeof(Recipient))
            {
                if (contrat.recipient.customerId != null)
                    customerId = contrat.recipient.customerId;
                if (contrat.recipient.phoneNumbers != null && contrat.recipient.phoneNumbers.Length != 0)
                {
                    foreach (Phonenumber1 phone in contrat.recipient.phoneNumbers)
                    {
                        if (phone.type == "LANDLINE")
                            telephonefixe = phone.number;
                        else if (phone.type == "MOBILE")
                            mobilephone = phone.number;
                    }
                }
            }
            else
            {
                if (trustedcon.customerId != null)
                    customerId = trustedcon.customerId;
                if (trustedcon.phoneNumbers != null && trustedcon.phoneNumbers.Length != 0)
                {
                    foreach (Phonenumber2 phone in trustedcon.phoneNumbers)
                    {
                        if (phone.type == "LANDLINE")
                            telephonefixe = phone.number;
                        else if (phone.type == "MOBILE")
                            mobilephone = phone.number;
                    }
                }
            }
        }
        #endregion
    }
}
