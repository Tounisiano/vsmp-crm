﻿
using System;

namespace VSMP_Leads
{
    public class Lead
    {
        public string Prospect { get; set; }
        public string Somme { get; set; }
        public string Modifié_le { get; set; }
        public string Prénom { get; set; }
        public string Nom_de_famille { get; set; }
        public string Mail { get; set; }
        public string Téléphone_professionnel { get; set; }
        public string Téléphone_mobile { get; set; }
        public string Adresse_1_Rue_1{ get; set; }      
        public string Adresse_1_Rue_2 { get; set; }
        public string Adresse_1_Rue_3 { get; set; }
        public string Adresse_1_Rue_4 { get; set; }
        public string Adresse_1_Ville { get; set; }
        public string Adresse_1_Code_postal { get; set; }
        public string Commentaire { get; set; }
        public string Statut_apport { get; set; }
        public string Motif_Abandonné { get; set; }
        public string Type_VSMP { get; set; }
        public string Ref_apport { get; set; }
        public string Date { get; set; }
        public string Ouvert { get; set; }
        public string Canal { get; set; }
        public string Type_de_prospect { get; set; }
        public string Statut_du_contrat { get; set; }
        public string Numéro_du_contrat { get; set; }
        public string Veille { get; set; }
        public string Type_apporteur { get; set; }
        public string Nom_de_lapporteur { get; set; }
        public string Prénom_de_lapporteur { get; set; }
        public string IDRH { get; set; }
        public string Code_Régate { get; set; }
        public string Type_contact_lié { get; set; }
        public string Nom_du_contact_2 { get; set; }
        public string Prénom_du_contact_2 { get; set; }
        public string Type_contact_lié_2 { get; set; }
        public string Téléphone_mobile_2 { get; set; }
        public string Téléphone_professionnel_2 { get; set; }
        public string Courrier_électronique_2 { get; set; }
        public string Adresse2_Rue1 { get; set; }
        public string Adresse2_Rue2 { get; set; }
        public string Adresse2_Rue3 { get; set; }
        public string Adresse_2_Rue_4 { get; set; }
        public string Adresse2_Code_postal { get; set; }
        public string Adresse2_Ville { get; set; }
        public string Nom_du_contact_3 { get; set; }
        public string Prénom_du_contact_3 { get; set; }
        public string Type_contact_lié_3 { get; set; }
        public string Téléphone_mobile_3 { get; set; }
        public string Téléphone_professionnel_3 { get; set; }
        public string Courrier_électronique_3 { get; set; }
        public string Adresse_3_Rue_1 { get; set; }
        public string Adresse_3_Rue_2 { get; set; }
        public string Adresse3_Rue3 { get; set; }
        public string Adresse_3_Rue_4 { get; set; }
        public string Adresse_3_Code_postal { get; set; }
        public string Adresse_3_Ville { get; set; }
        public string Action { get; set; }
        public string Date_limite { get; set; }
        public string Statut_de_laction { get; set; }
        public string Contact { get; set; }
        public string Commentaire_interne_CRC { get; set; }
    }



}
