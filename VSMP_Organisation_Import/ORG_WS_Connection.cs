﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Log;
using System.Xml;
using System.IO;

namespace VSMP_Organisation
{
    public static class ORG_WS_Connection
    {
        private static string username;
        private static string password;
        private static string responsedata = null;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        private static XmlDocument VSMP_Settings;

        public async static Task<string> GetContracts(string URL, string startDate, string EndDate)
        {
            try
            {
                VSMP_Settings = new XmlDocument();
                VSMP_Settings.Load(Xml_Settings_Path);
                username = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Organisation_Settings//Source_Orga_ID").InnerText;
                password = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Organisation_Settings//Source_Orga_Password").InnerText;
                using (HttpClient httpClient = new HttpClient())
                {
                    //Ajouter une authentification basique
                    var authByteArray = Encoding.ASCII.GetBytes(username + ":" + password);
                    var authString = Convert.ToBase64String(authByteArray);
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authString);
                    //Effectuer une requete GET
                    HttpResponseMessage response = await httpClient.GetAsync(URL);
                    LogHelper.Writer("response: " + response);
                    responsedata = await response.Content.ReadAsStringAsync();
                    LogHelper.Writer("response: " + response);

                    LogHelper.Writer(responsedata); // a supprimer apres
                }
            }
            catch(Exception ex)
            {
                LogHelper.Writer("Erreur lors de la récupération des données 'Organisation' du Web Service, Type d'erreur : " + Environment.NewLine + ex.ToString());
            }
            return responsedata;
        }
    }
}
