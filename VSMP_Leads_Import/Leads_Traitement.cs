﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Log;
using CRM_Connection;

namespace VSMP_Leads
{
    public class Leads_Traitement
    {
        #region variables statiques fixes
        private static OrganizationServiceProxy _orgService = null;
        private static Guid LeadId = Guid.Empty;
        private static string[] LeadFields = new string[] { "vsmp_refapport", "vsmp_numeroducontrat", "vsmp_veille", "firstname", "lastname", "mobilephone",
                                                            "telephone1","emailaddress1", "address1_line1", "address1_line2", "address1_line3", "vsmp_address1_line4", "address1_postalcode", "address1_city",
                                                            "address2_line1", "address2_line2", "address2_line3", "vsmp_address2_line4", "address2_postalcode",
                                                            "address2_city", "vsmp_nomducontact2", "vsmp_datedouverture", "vsmp_prenomducontact2", "vsmp_teelphonemobile2",
                                                            "vsmp_telephoneprofessionnel2", "vsmp_courrierelectronique2", "vsmp_nomducontact3", "vsmp_prenomducontact3", "vsmp_telephonemobile3",
                                                            "vsmp_telephoneprofessionnel3", "vsmp_courrierlectronique3", "vsmp_adresse3_rue1", "vsmp_adresse3_rue2", "vsmp_adresse3_rue3", "vsmp_address3_line4",
                                                            "vsmp_adresse3_ville", "vsmp_adresse3_codepostal", "vsmp_nomducontact", "vsmp_prenomducontact", "vsmp_idrh",
                                                            "vsmp_contact", "description", "vsmp_commentaire","vsmp_coderegate", "vsmp_date","estimatedclosedate", "vsmp_statutdelaction", "vsmp_statutprospect",
                                                            "vsmp_motifdabandon", "vsmp_typevsmp", "vsmp_canal", "vsmp_typedeprospect", "vsmp_statutducontrat","vsmp_typeapporteur", "vsmp_typecontact",
                                                            "vsmp_typecontact2", "vsmp_typecontact3", "vsmp_action" };
        private static List<Lead> Results = new List<Lead>();
        private static List<Entity> CRMLeads = new List<Entity>();
        private static List<Entity> CRMLeads_ToCreateOrUpdate = new List<Entity>();
        private static List<Entity> CRMBusinessUnits = new List<Entity>();
        private static EntityCollection CRMLeadsCollection = new EntityCollection();
        private static int NB_Errors = 0;
        private static string ArchivePath;
        private static string fileName;
        private static int NB_Leads_ToCreate = 0;
        private static int NB_Leads_ToUpdate = 0;
        private static string dirPath;
        private static List<string> files;
        private static bool Has_Changed;
        #endregion

        #region Batch Execute
        public static void BatchExecute(string[] args)
        {
            try
            {                
                //1-Créaations des logs
                LogHelper.CreateLog();

                //2-connexion CRM et récupération des records
                _orgService = CRMConnection.InitCRMConnection();
                _orgService.Timeout = new TimeSpan(0, 5, 0);

                if (_orgService == null)
                    return;
                LogHelper.WriteHeader("Nouvelle exécution, Debut du traitement... ");

                //3-Recherche du fichier CSV
                dirPath = Directory.GetCurrentDirectory() + @"\Source ADA\";
                files = new List<string>(Directory.EnumerateFiles(dirPath));

                //3-Si le fichier est trouvé alors effectuer le traitement de chargement
                if (files.Count != 0)                                       
                    LeadsImport();
                else                            
                    LogHelper.Writer("Aucun fichier Lead existant !");      //sinon, affichage d'un message, non existence de fichier apport
            }
            catch (Exception ex)
            {
                LogHelper.Writer("Erreur inattendue ! : " + Environment.NewLine + ex.ToString());
                NB_Errors++;
            }
            finally
            {
                LogHelper.WriteHeader(" Fin de l'import ");
            }
        }
        #endregion

        #region Récupération des records
        static void RecordsRetrieve()
        {
            try
            {
                CRMBusinessUnits = Leads_Application.GetRecords(_orgService, "businessunit", new string[] { "vsmp_code" });
                LogHelper.WriteHeader("Début de récupération des apports...");
                CRMLeads = Leads_Application.GetRecords(_orgService, "lead", LeadFields);
                if (CRMLeads == null)
                    CRMLeads = new List<Entity>();
                LogHelper.WriteHeader(string.Format("Fin de récupération des apports : {0} records récupérés", CRMLeads.Count));
            }
            catch(Exception ex)
            {
                throw new Exception("Erreur lors de la récupération des records",ex);
            }
        }
        #endregion

        #region Imports des leads
        static void LeadsImport()
        {
            //renseigner le nom du fichier apport retrouvé
            fileName = files[0].Substring(files[0].LastIndexOf("\\") + 1);
            //enregister le chemin du fichier
            dirPath = dirPath + fileName;
            //effectuer le mapping CRM/Excel
            Results = ExcelMapper.MapData(dirPath);
            //récupérer les records          
            RecordsRetrieve();
            //génerer les apports
            GenerateLeads();
            //effectuer un upsert sur CRM
            UpsertRecords(CRMLeads_ToCreateOrUpdate, CRMLeadsCollection, "lead");
            //archiver les fichier d'apports
            ArchieveFile();
        }
        #endregion 

        #region Traitement des Leads
        static void GenerateLeads()
        {
            LogHelper.WriteHeader("Début du traitement des apports, génération des records" );
            foreach (Lead lead in Results)                                          //parcours de la liste d'apports générée
            {
                try
                {                   
                    if (CRMLeads != null && CRMLeads.Any(entity => entity.GetAttributeValue<string>("vsmp_refapport") == lead.Ref_apport))      //si l'apport existe, le mettre à jour
                    {
                        Entity retrievedLead = CRMLeads.Find(entity => entity.GetAttributeValue<string>("vsmp_refapport") == lead.Ref_apport);
                        CRMLeads.Remove(retrievedLead);
                        if (CRMLeads_ToCreateOrUpdate.Any(entity => entity.GetAttributeValue<string>("vsmp_refapport") == lead.Ref_apport))
                            CRMLeads_ToCreateOrUpdate.Remove(CRMLeads_ToCreateOrUpdate.Find(entity => entity.GetAttributeValue<string>("vsmp_refapport") == lead.Ref_apport));
                        Leads_Application.MettreAJourLeads(lead, retrievedLead, CRMBusinessUnits, out Has_Changed);
                        if (Has_Changed == true)
                            CRMLeads_ToCreateOrUpdate.Add(retrievedLead);
                        CRMLeads.Add(retrievedLead);
                        NB_Leads_ToUpdate++;
                    }
                    else                                                                                                                        //sinon, le créer et le renseigner
                    {
                        Entity NewLead = new Entity("lead");
                        Leads_Application.RenseignerLead(NewLead, lead, CRMBusinessUnits);
                        CRMLeads.Add(NewLead);
                        CRMLeads_ToCreateOrUpdate.Add(NewLead);
                        NB_Leads_ToCreate++;
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.Writer("Début Erreur" + Environment.NewLine);
                    LogHelper.Writer("Erreur lors du traitement de l'apport numero : " + lead.Ref_apport + Environment.NewLine + "Type d'erreur : " + ex.ToString() + Environment.NewLine);
                    LogHelper.Writer("Fin Erreur" + Environment.NewLine);
                    NB_Errors++;
                }
            }
            LogHelper.WriteHeader(string.Format("Fin du traitement des apports, Nombre d'erreurs : {0}, Nombre de nouveaux apports à créer : {1}, Nombre d'apport à mettre à jour : {2}",
                NB_Errors, CRMLeads_ToCreateOrUpdate.Count(entity=>entity.Id==Guid.Empty), CRMLeads_ToCreateOrUpdate.Count(entity => entity.Id != Guid.Empty)));
        }
        #endregion

        #region upsert des records
        static void UpsertRecords(List<Entity> records, EntityCollection collection, string logicalName)
        {
            if (records != null && records.Count != 0)
            {
                //Création d'une collection d'entités 
                foreach (Entity entity in records)
                    collection.Entities.Add(entity);
                collection.EntityName = logicalName;
                //Upsert de records sur CRM
                Leads_Application.CreateMultiple(_orgService, collection);
            }
            else
                LogHelper.Writer("Aucun record apport à créer ou mettre à jour");
        }
        #endregion

        #region Archivage du fichier d'apports
        static void ArchieveFile()
        {
            try
            {
                //Assignation du nom du dossier d'archive des apports
                ArchivePath = Directory.GetCurrentDirectory() + @"\Archive ADA\";
                //si ce dossier n'existe pas, alors le créer.
                if (!Directory.Exists(ArchivePath))
                    Directory.CreateDirectory(ArchivePath);
                //déplacer le fichier Excel des apports depuis le dossier de source vers le dossier d'archive
                File.Move(dirPath, ArchivePath + fileName);
            }
            catch(Exception ex)
            {
                LogHelper.Writer("Erreur lors de l'archivage du fichier d'apports, type d'erreur : " + Environment.NewLine + ex.ToString());
            }
        }
        #endregion
    }
}
