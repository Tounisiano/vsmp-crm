﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using Log;
using System.Linq;

namespace VSMP_Organisation
{
    public class ORG_Application
    {
        #region Récupération de tous les records d'une entité donnée depuis le CRM
        public static List<Entity> GetRecords(IOrganizationService _orgService, string EntityLogicalName, string[] CRMFields)
        {
            List<EntityCollection> recordsRetrieved = new List<EntityCollection>();
            int querycount = 5000;
            int pagenumber = 1;
            List<Entity> CRMRecords = new List<Entity>();
            QueryExpression query = new QueryExpression(EntityLogicalName)
            { ColumnSet = new ColumnSet(CRMFields) };
            query.PageInfo = new PagingInfo();
            query.PageInfo.Count = querycount;
            query.PageInfo.PageNumber = pagenumber;
            query.PageInfo.PagingCookie = null;

            while (true)
            {
                EntityCollection newCollection = _orgService.RetrieveMultiple(query);
                recordsRetrieved.Add(newCollection);
                if (newCollection.MoreRecords)
                {
                    query.PageInfo.PageNumber++;
                    query.PageInfo.PagingCookie = newCollection.PagingCookie;
                }
                else
                    break;
            }
            if (recordsRetrieved == null || recordsRetrieved.Count < 1 || recordsRetrieved[0].Entities == null)
                return null;
            else
            {
                foreach (EntityCollection collection in recordsRetrieved)
                {
                    foreach (Entity entity in collection.Entities)
                        CRMRecords.Add(entity);
                }
                return CRMRecords;
            }
        }
        #endregion

        #region renseigner les champs d'une division
        public static void CreateOrUpdateBU(Entity BU, Organisation organisation, List<Entity> CRMBusinessUnits)
        {
            Entity RootBU;
            Guid ParentID;
            BU.Attributes["vsmp_code"] = organisation.CODE_REGATE;
            BU.Attributes["name"] = organisation.LIBELLE;
            
            if(organisation.CODE_REGATE_ENTITE_RATTACHEMENT != string.Empty && GetBU(organisation.CODE_REGATE_ENTITE_RATTACHEMENT, CRMBusinessUnits) != Guid.Empty 
               && organisation.LIBELLE_COURT_TYPE != "DEX")
            {
                ParentID = GetBU(organisation.CODE_REGATE_ENTITE_RATTACHEMENT, CRMBusinessUnits);
                BU.Attributes["parentbusinessunitid"] = new EntityReference("businessunit", ParentID);
            }

            switch (organisation.LIBELLE_COURT_TYPE)
            {
                case "DEX":
                    BU.Attributes["vsmp_typededivision"] = new OptionSetValue(688760000);
                    RootBU = CRMBusinessUnits.Find(entity => ((OptionSetValue)entity["vsmp_typededivision"]).Value == 688760000);
                    BU.Attributes["parentbusinessunitid"] = new EntityReference("businessunit", ((EntityReference)RootBU["parentbusinessunitid"]).Id);
                    break;
                case "DR":
                    BU.Attributes["vsmp_typededivision"] = new OptionSetValue(688760001);
                    break;
                case "DT":
                    BU.Attributes["vsmp_typededivision"] = new OptionSetValue(688760002);
                    break;
                case "BC":
                    BU.Attributes["vsmp_typededivision"] = new OptionSetValue(688760003);
                    break;
                case "BP":
                    BU.Attributes["vsmp_typededivision"] = new OptionSetValue(688760004);
                    break;
            }
        }
        #endregion

        #region Recherche d'une division par code regate
        public static Guid GetBU(string CodeRegate, List<Entity> CRMBusinessUnits)
        {
            if (CRMBusinessUnits != null && CRMBusinessUnits.Any(entity => entity.GetAttributeValue<string>("vsmp_code") == CodeRegate))
                return CRMBusinessUnits.Find(entity => entity.GetAttributeValue<string>("vsmp_code") == CodeRegate).Id;
            else
                return Guid.Empty;
        }
        #endregion

        #region Création multiple des records
        public static void CreateMultiple(IOrganizationService _orgService, EntityCollection input)
        {
            int i = 1;
            int nb_records = 1;
            int nb_responses = 0;
            ExecuteMultipleResponse responseWithResults = null;
            ExecuteMultipleRequest requestWithResults = new ExecuteMultipleRequest()
            {
                // Assigner les paramètre définissant le comportement de l'exécution : continuer en cas d'erreur, retourner des réponses
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                Requests = new OrganizationRequestCollection()
            };
            LogHelper.Writer(string.Format("Début de l'upsert CRM des records de l'entité {0}, {1} records à traiter en total", input.EntityName, input.Entities.Count));
            foreach (var entity in input.Entities)
            {
                UpsertRequest upsertrequest = new UpsertRequest { Target = entity };
                requestWithResults.Requests.Add(upsertrequest);                    
                if (i % 500 == 0 || i == input.Entities.Count)          //upsert des records par batch de 500 records
                {
                    try
                    {
                        //Exécuter toutes les requetes upsert de la collection par un seul appel web.                       
                        responseWithResults = (ExecuteMultipleResponse)_orgService.Execute(requestWithResults);
                        if (responseWithResults.Responses.Count > 0)
                        {                           
                            foreach (var responseItem in responseWithResults.Responses)
                            {
                                if (responseItem.Fault != null)
                                    DisplayFault(requestWithResults.Requests[responseItem.RequestIndex], responseItem.RequestIndex, responseItem.Fault);
                            }
                        }
                        requestWithResults.Requests = null;
                        requestWithResults.Requests = new OrganizationRequestCollection();
                        LogHelper.Writer(string.Format("# {0} recrods traités", nb_records));
                        nb_records = 0;
                        nb_responses += responseWithResults.Responses.Count;                      
                    }
                    catch(Exception ex)
                    {
                        LogHelper.Writer("UPSERT ERROR, type d'erreur : " + ex.ToString());
                    }
                }
                i++;
                nb_records++;
            }
            LogHelper.Writer(string.Format("# nombre de réponses à l'upsert : {0}", nb_responses));
            LogHelper.Writer(string.Format("Fin de l'upsert, {0} records traités" + Environment.NewLine, i - 1));
        }
        
        #endregion

        #region affichage des erreurs upsert
        static private void DisplayFault(OrganizationRequest organizationRequest, int count, OrganizationServiceFault organizationServiceFault)
        {
            LogHelper.Writer(string.Format("### une erreur est survenue dans le traitement de la requete {1},ç l'index {0} dans la collection d'entités avec le message suivant : {2}", count + 1,
                organizationRequest.RequestName, organizationServiceFault.Message));
        }
        #endregion
    }
}
