﻿using System;


namespace VSMP_Contracts
{
    public class Contract
    {
        public string nextResults { get; set; }
        public string href { get; set; }
        public Result[] results { get; set; }
    }

    public class Result : IComparable<Result>
    {
        public string id { get; set; }
        public DateTime? dateModification { get; set; }
        public bool active { get; set; }
        public string href { get; set; }
        public string system { get; set; }
        public int version { get; set; }
        public string contractNumber { get; set; }
        public Legalinfo legalInfo { get; set; }
        public Payment payment { get; set; }
        public Delivery delivery { get; set; }
        public Subscriber subscriber { get; set; }
        public Recipient recipient { get; set; }
        public Trustedcontact[] trustedContact { get; set; }
        public bool remoteAssistance { get; set; }
        public bool ccuCreatedUser { get; set; }
        public Metadata metadata { get; set; }

        public int CompareTo(Result obj)
        {
            return version.CompareTo(obj.version);
        }
    }

    public class Legalinfo
    {
        public bool modified { get; set; }
        public DateTime? beginDate { get; set; }
        public int contractDurationInMonths { get; set; }
        public DateTime? terminationRequestDate { get; set; }
        public string signed { get; set; }
        public string terminationReason { get; set; }
        public string contractStatus { get; set; }
    }

    public class Payment
    {
        public bool modified { get; set; }
        public string type { get; set; }
        public string BIC { get; set; }
        public string IBAN { get; set; }
        public string accountOwner { get; set; }
        public string promotionCode { get; set; }
        public decimal htPriceInCents { get; set; }
        public decimal tvaAmountInCents { get; set; }
        public decimal ttcPriceInCents { get; set; }
        public string signed { get; set; }
        public string RUM { get; set; }
    }

    public class Delivery
    {
        public bool modified { get; set; }
        public string formula { get; set; }
        public string[] visitDays { get; set; }
        public DateTime? startDate { get; set; }
        public bool suspended { get; set; }
        public DateTime? suspensionDate { get; set; }
        public string suspensionReason { get; set; }
    }

    public class Subscriber
    {
        public bool modified { get; set; }
        public string customerId { get; set; }
        public string civility { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public DateTime? birthDate { get; set; }
        public Phonenumber[] phoneNumbers { get; set; }
        public string mail { get; set; }
        public string adressL2 { get; set; }
        public string adressL3 { get; set; }
        public string adressL4 { get; set; }
        public string adressL5 { get; set; }
        public string adressL6 { get; set; }
        public string countryCode { get; set; }
        public bool offlineClient { get; set; }
    }

    public class Phonenumber
    {
        public string type { get; set; }
        public string number { get; set; }
    }

    public class Recipient
    {
        public bool modified { get; set; }
        public string customerId { get; set; }
        public string civility { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public DateTime? birthDate { get; set; }
        public Phonenumber1[] phoneNumbers { get; set; }
        public string mail { get; set; }
        public string adressL2 { get; set; }
        public string adressL3 { get; set; }
        public string adressL4 { get; set; }
        public string adressL5 { get; set; }
        public string adressL6 { get; set; }
        public string doctorPhone { get; set; }
        public string phoneInstallationType { get; set; }
        public string doctorName { get; set; }
        public string doctorCivility { get; set; }
        public string[] impairements { get; set; }
        public string furtherInformation { get; set; }
        public string subscribersRelationship { get; set; }
       

    }

    public class RecipientMeta
    {
        public bool? secondLocket { get; set; }
    }

        public class Phonenumber1
    {
        public string type { get; set; }
        public string number { get; set; }
    }

    public class Metadata
    {
        public Retailinformations retailInformations { get; set; }
        public bool? test { get; set; }
        public RecipientMeta recipient { get; set; }
        public Crc crc { get; set; }

    }

    public class Retailinformations
    {
        public string regateCode { get; set; }
    }

    public class Crc
    {
        public string leadCode { get; set; }
    }

    public class Trustedcontact
    {
        public bool modified { get; set; }
        public string order { get; set; }
        public string customerId { get; set; }
        public string civility { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public DateTime? birthDate { get; set; }
        public Phonenumber2[] phoneNumbers { get; set; }
        public string mail { get; set; }
        public int travelTimeInMinutes { get; set; }
        public string contactType { get; set; }
        public bool hasKeys { get; set; }
        public string adressL2 { get; set; }
        public string adressL3 { get; set; }
        public string adressL4 { get; set; }
        public string adressL5 { get; set; }
        public string adressL6 { get; set; }
        public bool over18 { get; set; }
    }

    public class Phonenumber2
    {
        public string type { get; set; }
        public string number { get; set; }
    }


}










