﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.Configuration;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System.IO;
using Microsoft.Xrm.Sdk.Messages;
using System.ServiceModel;
using System.Xml;
using Log;
using CRM_Connection;

namespace VSMP_Graph_Statut
{
    class Program
    {

        #region variables
        private static OrganizationServiceProxy _orgService = null;
        private static string Xml_Settings_Path = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName + @"\VSMP_Settings_File.xml";
        //private static XmlDocument VSMP_Settings;
       
        #endregion variables


        static void Main(string[] args)
        {

            try
            {

                //1-Créaations des logs
                LogHelper.CreateLog();

                //2-connexion CRM et récupération des parametres du fichier de configuration
                _orgService = CRMConnection.InitCRMConnection();
                if (_orgService == null)
                    return;
                LogHelper.WriteHeader("Nouvelle exécution, Debut du traitement... ");

                //VSMP_Settings = new XmlDocument();
                //VSMP_Settings.Load(Xml_Settings_Path);
                //string fileName = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Geolocalisation_Import_Settings//FileName").InnerText;
                //string filePath = VSMP_Settings.DocumentElement.SelectSingleNode("//VSMP_Geolocalisation_Import_Settings/FilePath").InnerText;
                //string fullPath = filePath + fileName;

                DateTime dtYesterday = DateTime.Now.Date.AddDays(-1).Date;
                EntityCollection tobeupdated = new EntityCollection();
              


                #region GetContracts
                //Create a query expression specifying the link entity alias and the columns of the link entity that you want to return
                QueryExpression q1 = new QueryExpression();
                q1.EntityName = "vsmp_contrat";
                ColumnSet cols = new ColumnSet("vsmp_contratid", "vsmp_coderegate", "vsmp_name", "vsmp_statut", "vsmp_datedesouscription", "vsmp_datedecreation", "vsmp_datedesuspension", "vsmp_datedactivation", "vsmp_datederesiliation", "vsmp_statutcontratvsmp3_id");
                q1.ColumnSet = cols;
                OrderExpression order1 = new OrderExpression("vsmp_name", OrderType.Ascending);
                q1.Orders.Add(order1);


                LogHelper.WriteHeader("Retreive Contracts done!");

                EntityCollection contractscollection = _orgService.RetrieveMultiple(q1);
                #endregion


                #region GetStatut
                //Create a query expression specifying the link entity alias and the columns of the link entity that you want to return
                QueryExpression q2 = new QueryExpression();
                q2.EntityName = "vsmp_statutscontatsvsmp3";
                ColumnSet cols2 = new ColumnSet("vsmp_statutscontatsvsmp3id", "vsmp_name");
                q2.ColumnSet = cols2;

                LogHelper.WriteHeader("Retreive Status done!");

                EntityCollection statuscollection = _orgService.RetrieveMultiple(q2);
                #endregion



                #region Renseignement du champ
                foreach (Entity contrat in contractscollection.Entities)
                {
                    if (contrat.Contains("vsmp_name") && contrat["vsmp_name"] != null)
                    {

                        int statut = ((OptionSetValue)contrat["vsmp_statut"]).Value;




                        if (contrat.Contains("vsmp_datedecreation") && contrat["vsmp_datedecreation"] != null && statut == 688760000 && ((DateTime)contrat["vsmp_datedecreation"]).Date == dtYesterday)
                        {
                            Entity statutContrats = statuscollection.Entities.FirstOrDefault(x => x["vsmp_name"].ToString() == "Créés ce jour");
                            contrat["vsmp_statutcontratvsmp3_id"] = new EntityReference("vsmp_statutscontatsvsmp3", statutContrats.Id);
                            tobeupdated.Entities.Add(contrat);
                            LogHelper.Writer("Créés ce jour");
                        }
                        else if (contrat.Contains("vsmp_datedesouscription") && contrat["vsmp_datedesouscription"] != null &&  statut == 688760001 && ((DateTime)contrat["vsmp_datedesouscription"]).Date == dtYesterday)
                        {
                            Entity statutContrats = statuscollection.Entities.FirstOrDefault(x => x["vsmp_name"].ToString() == "Souscrits ce jour");
                            contrat["vsmp_statutcontratvsmp3_id"] = new EntityReference("vsmp_statutscontatsvsmp3", statutContrats.Id);
                            tobeupdated.Entities.Add(contrat);
                            LogHelper.Writer("souscrit ce jour");
                        }
                        else if (contrat.Contains("vsmp_datedactivation") && contrat["vsmp_datedactivation"] != null && statut == 688760002 && ((DateTime)contrat["vsmp_datedactivation"]).Date == dtYesterday)
                        {
                            Entity statutContrats = statuscollection.Entities.FirstOrDefault(x => x["vsmp_name"].ToString() == "Activés ce jour");
                            contrat["vsmp_statutcontratvsmp3_id"] = new EntityReference("vsmp_statutscontatsvsmp3", statutContrats.Id);
                            tobeupdated.Entities.Add(contrat);
                            LogHelper.Writer("Activé ce jour");

                        }
                        else if ((contrat.Contains("vsmp_datedesuspension") && contrat["vsmp_datedesuspension"] != null &&  statut == 688760003 && ((DateTime)contrat["vsmp_datedesuspension"]).Date == dtYesterday) || (contrat.Contains("vsmp_datederesiliation") && contrat["vsmp_datederesiliation"] != null && statut == 688760004 && ((DateTime)contrat["vsmp_datederesiliation"]).Date == dtYesterday))
                        {
                            Entity statutContrats = statuscollection.Entities.FirstOrDefault(x => x["vsmp_name"].ToString() == "Suspendus / en cours d'annulation ce jour");
                            contrat["vsmp_statutcontratvsmp3_id"] = new EntityReference("vsmp_statutscontatsvsmp3", statutContrats.Id);
                            tobeupdated.Entities.Add(contrat);
                            LogHelper.Writer("Suspendus / en cours ce jour");
                        }
                        else
                        {
                           if (contrat.Contains("vsmp_statutcontratvsmp3_id") && contrat["vsmp_statutcontratvsmp3_id"] != null)
                            {
                                contrat["vsmp_statutcontratvsmp3_id"] = null;
                                tobeupdated.Entities.Add(contrat);
                            }
                 

                        }

                    }


                }
                LogHelper.Writer("Number of rec to update :" + tobeupdated.Entities.Count);
                #endregion

                #region Mise a jour de l'entité
                // Execute BulkUpdate on the tobeupdated entity colection. 
                BulkUpdate(_orgService, tobeupdated);
                #endregion

            }
            catch (Exception ex)
            {

                LogHelper.Writer("### La connexion sur CRM n'a pas abouti, type d'erreur : " + Environment.NewLine + ex.ToString());

                LogHelper.Writer("Error: " + ex.Message);
            }

        }

        #region bulk update
        /// <summary>
        /// Call this method for bulk update
        /// </summary>
        /// <param name="service">Org Service</param>
        /// <param name="entities">Collection of entities to Update</param>
        public static void BulkUpdate(IOrganizationService service, EntityCollection entities)
        {
            // Create an ExecuteMultipleRequest object.
            var multipleRequest = new ExecuteMultipleRequest()
            {
                // Assign settings that define execution behavior: continue on error, return responses. 
                Settings = new ExecuteMultipleSettings()
                {
                    ContinueOnError = true,
                    ReturnResponses = true
                },
                // Create an empty organization request collection.
                Requests = new OrganizationRequestCollection()
            };

            // Add a UpdateRequest for each entity to the request collection.
            foreach (var entity in entities.Entities)
            {
                UpdateRequest updateRequest = new UpdateRequest { Target = entity };
                multipleRequest.Requests.Add(updateRequest);
            }

            // Execute all the requests in the request collection using a single web method call.
            ExecuteMultipleResponse multipleResponse = (ExecuteMultipleResponse)service.Execute(multipleRequest);

        }
        #endregion bulk update
    }
}
