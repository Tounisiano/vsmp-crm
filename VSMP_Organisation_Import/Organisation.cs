﻿using System;

namespace VSMP_Organisation
{
    public class Organisation : IComparable<Organisation>
    {
        public string CODE_REGATE { get; set; }
        public string LIBELLE { get; set; }
        public string LIBELLE_COURT_TYPE { get; set; }
        public string CODE_REGATE_ENTITE_RATTACHEMENT { get; set; }
        public string LIBELLE_ENTITE_RATTACHEMENT { get; set; }
        public string LIBELLE_COURT_TYPE_ENTITE_RATTACHEMENT { get; set; }
        
        public int CompareTo(Organisation other)
        {
            if (this.LIBELLE_COURT_TYPE == "DR" && other.LIBELLE_COURT_TYPE == "DEX" || this.LIBELLE_COURT_TYPE == "ZM" && other.LIBELLE_COURT_TYPE == "DEX"
                || this.LIBELLE_COURT_TYPE == "BC" && other.LIBELLE_COURT_TYPE == "DEX" || this.LIBELLE_COURT_TYPE == "BP" && other.LIBELLE_COURT_TYPE == "DEX"
                || this.LIBELLE_COURT_TYPE == "ZM" && other.LIBELLE_COURT_TYPE == "DR" || this.LIBELLE_COURT_TYPE == "BC" && other.LIBELLE_COURT_TYPE == "DR"
                || this.LIBELLE_COURT_TYPE == "BP" && other.LIBELLE_COURT_TYPE == "DR" || this.LIBELLE_COURT_TYPE == "BC" && other.LIBELLE_COURT_TYPE == "ZM"
                || this.LIBELLE_COURT_TYPE == "BP" && other.LIBELLE_COURT_TYPE == "ZM" || this.LIBELLE_COURT_TYPE == "BP" && other.LIBELLE_COURT_TYPE == "BC")
                return -1;
            else if (this.LIBELLE_COURT_TYPE == other.LIBELLE_COURT_TYPE) return 0;
            else return 1;
        }   
    }
}
